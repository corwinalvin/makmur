-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 24 Okt 2017 pada 02.12
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `makmur`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `brand` varchar(100) NOT NULL,
  `logo` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `brands`
--

INSERT INTO `brands` (`id`, `brand`, `logo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Maktec', '"\\/media\\/images\\/shares\\/client-2.jpg"', '2010-11-17 07:38:00', '2017-10-23 03:52:15', '0000-00-00 00:00:00'),
(2, 'Bitec', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:37:14', '2017-10-23 00:37:14', '0000-00-00 00:00:00'),
(3, 'Ken', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:37:19', '2017-10-23 00:37:29', '0000-00-00 00:00:00'),
(4, 'Skil', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:37:42', '2017-10-23 00:37:42', '0000-00-00 00:00:00'),
(5, 'Multipro', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:37:47', '2017-10-23 00:37:47', '0000-00-00 00:00:00'),
(6, 'Apach', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:37:52', '2017-10-23 00:37:52', '0000-00-00 00:00:00'),
(7, 'Panasonic', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:37:57', '2017-10-23 00:37:57', '0000-00-00 00:00:00'),
(8, 'Sanyo', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:38:03', '2017-10-23 00:38:03', '0000-00-00 00:00:00'),
(9, 'Karcher', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:38:11', '2017-10-23 00:38:11', '0000-00-00 00:00:00'),
(10, 'Shimizu', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:38:25', '2017-10-23 00:38:25', '0000-00-00 00:00:00'),
(11, 'Bosch', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:38:36', '2017-10-23 00:38:36', '0000-00-00 00:00:00'),
(12, 'Union', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:38:42', '2017-10-23 00:38:42', '0000-00-00 00:00:00'),
(13, 'Mitsubishi', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:38:58', '2017-10-23 00:38:58', '0000-00-00 00:00:00'),
(14, 'Showfou', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:39:05', '2017-10-23 00:39:05', '0000-00-00 00:00:00'),
(15, 'Wilo', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:39:11', '2017-10-23 00:39:11', '0000-00-00 00:00:00'),
(16, 'Wasser', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:39:18', '2017-10-23 00:39:18', '0000-00-00 00:00:00'),
(17, 'Niagara', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:39:28', '2017-10-23 00:39:28', '0000-00-00 00:00:00'),
(18, 'Honda', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:39:37', '2017-10-23 00:39:37', '0000-00-00 00:00:00'),
(19, 'Daiden', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:39:45', '2017-10-23 00:39:45', '0000-00-00 00:00:00'),
(20, 'Wim', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:39:58', '2017-10-23 00:39:58', '0000-00-00 00:00:00'),
(21, 'AEG', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:40:07', '2017-10-23 00:40:07', '0000-00-00 00:00:00'),
(22, 'Sanchin', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:40:16', '2017-10-23 00:40:16', '0000-00-00 00:00:00'),
(23, 'Swan', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:40:26', '2017-10-23 00:40:26', '0000-00-00 00:00:00'),
(24, 'Tasco', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:40:32', '2017-10-23 00:40:32', '0000-00-00 00:00:00'),
(25, 'Agrindo', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:40:38', '2017-10-23 00:40:38', '0000-00-00 00:00:00'),
(26, 'Quick', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:40:45', '2017-10-23 00:40:45', '0000-00-00 00:00:00'),
(27, 'In-da', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:40:51', '2017-10-23 00:40:51', '0000-00-00 00:00:00'),
(28, 'Kubota', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:41:01', '2017-10-23 00:41:01', '0000-00-00 00:00:00'),
(29, 'Tiger', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:41:06', '2017-10-23 00:41:06', '0000-00-00 00:00:00'),
(30, 'Tiger', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:41:16', '2017-10-23 00:41:35', '2017-10-23 00:41:35'),
(31, 'Tenka', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:42:17', '2017-10-23 00:42:17', '0000-00-00 00:00:00'),
(32, 'ATT', '"\\/media\\/images\\/shares\\/client-3.jpg"', '2017-10-23 00:42:22', '2017-10-23 00:42:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `name` varchar(60) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `picture` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `slug`, `picture`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 0, 'Air Compressor, Accesories & Parts', 'air-compressor-accesories-parts', '', '2017-09-13 20:50:55', '2017-09-13 20:50:55', NULL),
(4, 0, 'Abbrasive & Finishing Product', 'abbrasive-finishing-product', '', '2017-09-13 20:52:06', '2017-09-13 20:52:06', NULL),
(5, 3, 'Unloading', 'unloading', '', '2017-09-13 20:52:16', '2017-09-13 20:52:16', NULL),
(6, 3, 'Automatic', 'automatic', '', '2017-09-13 20:52:26', '2017-09-13 20:52:26', NULL),
(7, 3, 'Portable', 'portable', '', '2017-09-13 20:52:36', '2017-09-13 20:52:36', NULL),
(8, 3, 'Oil Less', 'oil-less', '', '2017-09-13 20:52:46', '2017-09-13 20:52:46', NULL),
(9, 3, 'Screw', 'screw', '', '2017-09-13 20:52:56', '2017-09-13 20:52:56', NULL),
(10, 3, 'Accessories', 'accessories', '', '2017-09-13 20:53:11', '2017-09-13 20:53:11', NULL),
(11, 3, 'Parts', 'parts', '', '2017-09-13 20:53:20', '2017-09-13 20:53:20', NULL),
(12, 4, 'Grinding Wheels', 'grinding-wheels', '', '2017-09-13 20:54:22', '2017-09-13 20:54:22', NULL),
(13, 4, 'Cutting Wheels', 'cutting-wheels', '', '2017-09-13 20:54:31', '2017-09-13 20:54:31', NULL),
(14, 4, 'Vitrified', 'vitrified', '', '2017-09-13 20:54:44', '2017-09-13 20:54:44', NULL),
(15, 4, 'Industrial Brush', 'industrial-brush', '', '2017-09-13 20:54:54', '2017-09-13 20:54:54', NULL),
(16, 4, 'File', 'file', '', '2017-09-13 20:55:21', '2017-09-13 20:55:21', NULL),
(17, 4, 'Mounted', 'mounted', '', '2017-09-13 20:55:30', '2017-09-13 20:55:30', NULL),
(18, 4, 'Dresser', 'dresser', '', '2017-09-13 20:55:36', '2017-09-13 20:55:36', NULL),
(19, 4, 'Oil Stone', 'oil-stone', '', '2017-09-13 20:55:45', '2017-09-13 20:55:45', NULL),
(20, 4, 'Paper Sand', 'paper-sand', '', '2017-09-13 20:55:54', '2017-09-13 20:56:27', NULL),
(21, 4, 'Diamond Grinding', 'diamond-grinding', '', '2017-09-13 20:56:03', '2017-09-13 20:56:03', NULL),
(22, 4, 'Paper Sand Roll', 'paper-sand-roll', '', '2017-09-13 20:56:16', '2017-09-13 20:56:16', NULL),
(23, 4, 'Honing', 'honing', '', '2017-09-13 20:56:34', '2017-09-13 20:56:34', NULL),
(24, 4, 'Burrs', 'burrs', '', '2017-09-13 20:56:43', '2017-09-13 20:56:43', NULL),
(25, 4, 'Resin Small', 'resin-small', '', '2017-09-13 20:56:51', '2017-09-13 20:56:51', NULL),
(26, 4, 'Sponge', 'sponge', '', '2017-09-13 20:56:58', '2017-09-13 20:56:58', NULL),
(27, 4, 'Wool', 'wool', '', '2017-09-13 20:57:04', '2017-09-13 20:57:04', NULL),
(28, 0, 'Alternator Generator, Welding & Electro Motor', 'alternator-generator-welding-electro-motor', '', '2017-09-13 20:57:52', '2017-09-13 20:57:52', NULL),
(29, 0, 'Assembly', 'assembly', '', '2017-09-13 20:58:12', '2017-09-13 20:58:12', NULL),
(30, 28, 'Alternator Generator', 'alternator-generator', '', '2017-09-13 20:58:25', '2017-09-13 20:58:25', NULL),
(31, 28, 'Alternator Welding', 'alternator-welding', '', '2017-09-13 20:58:36', '2017-09-13 20:58:36', NULL),
(32, 28, 'Electro Motor', 'electro-motor', '', '2017-09-13 20:58:50', '2017-09-13 20:58:50', NULL),
(33, 0, 'Air Tools', 'air-tools', '', '2017-09-13 20:58:56', '2017-09-13 20:58:56', NULL),
(35, 33, 'Parts', 'parts', '', '2017-09-13 20:59:14', '2017-09-13 20:59:14', NULL),
(36, 0, 'Automotives', 'automotives', '', '2017-09-13 20:59:24', '2017-09-13 20:59:24', NULL),
(37, 0, 'Brush Cutter, Lawn Mower & Parts', 'brush-cutter-lawn-mower-parts', '', '2017-09-13 20:59:53', '2017-09-13 20:59:53', NULL),
(38, 0, 'Chain, Bearing, Belt & V-Belt', 'chain-bearing-belt-vbelt', '', '2017-09-13 21:00:09', '2017-09-13 21:00:09', NULL),
(39, 0, 'Cleaning Equipments & Parts', 'cleaning-equipments-parts', '', '2017-09-13 21:00:25', '2017-09-13 21:00:25', NULL),
(40, 0, 'Construction Equipments & Parts', 'construction-equipments-parts', '', '2017-09-13 21:00:35', '2017-09-13 21:00:35', NULL),
(41, 0, 'Chainsaw & Parts', 'chainsaw-parts', '', '2017-09-13 21:00:45', '2017-09-13 21:00:45', NULL),
(42, 0, 'Cutting Tools', 'cutting-tools', '', '2017-09-13 21:00:50', '2017-09-13 21:00:50', NULL),
(43, 0, 'Diesel Engine & Parts', 'diesel-engine-parts', '', '2017-09-13 21:00:59', '2017-09-13 21:00:59', NULL),
(44, 0, 'Electrical Equipments', 'electrical-equipments', '', '2017-09-13 21:01:08', '2017-09-13 21:01:08', NULL),
(45, 0, 'Etc', 'etc', '', '2017-09-13 21:01:14', '2017-09-13 21:01:14', NULL),
(46, 0, 'Gasoline Engine, Accesories  & Parts', 'gasoline-engine-accesories-parts', '', '2017-09-13 21:01:31', '2017-09-13 21:01:31', NULL),
(47, 0, 'Handling & Lifting Equipments', 'handling-lifting-equipments-', '', '2017-09-13 21:01:50', '2017-09-13 21:01:50', NULL),
(48, 0, 'Hoses', 'hoses', '', '2017-09-13 21:01:54', '2017-09-13 21:01:54', NULL),
(49, 0, 'Hand Tractors & Parts', 'hand-tractors-parts', '', '2017-09-13 21:02:07', '2017-09-13 21:02:07', NULL),
(50, 0, 'Measuring Equipments', 'measuring-equipments', '', '2017-09-13 21:02:16', '2017-09-13 21:02:16', NULL),
(51, 0, 'Machinery & Parts', 'machinery-parts', '', '2017-09-13 21:02:24', '2017-09-13 21:02:24', NULL),
(52, 0, 'Material', 'material', '', '2017-09-13 21:02:40', '2017-09-13 21:02:40', NULL),
(53, 0, 'Oils & Chemical Product', 'oils-chemical-product', '', '2017-09-13 21:02:53', '2017-09-13 21:02:53', NULL),
(54, 0, 'Packaging', 'packaging', '', '2017-09-13 21:02:56', '2017-09-13 21:02:56', NULL),
(55, 0, 'Power Sprayer Knapsack & Parts', 'power-sprayer-knapsack-parts', '', '2017-09-13 21:03:21', '2017-09-13 21:03:21', NULL),
(56, 0, 'Plastic Products', 'plastic-products', '', '2017-09-13 21:03:28', '2017-09-13 21:03:28', NULL),
(57, 0, 'Power Tools, Accesories & Parts', 'power-tools-accesories-parts', '', '2017-09-13 21:03:41', '2017-09-13 21:03:41', NULL),
(58, 0, 'Safety Equipments', 'safety-equipments', '', '2017-09-13 21:03:49', '2017-09-13 21:03:49', NULL),
(59, 0, 'Seal, Packing & Rubber Product', 'seal-packing-rubber-product', '', '2017-09-13 21:04:03', '2017-09-13 21:04:03', NULL),
(60, 0, 'Tractor & Parts', 'tractor-parts', '', '2017-09-13 21:04:08', '2017-09-13 21:04:08', NULL),
(61, 0, 'Tools', 'tools', '', '2017-09-13 21:04:15', '2017-09-13 21:04:15', NULL),
(62, 0, 'Welding Electrodes & Equipments', 'welding-electrodes-equipments', '', '2017-09-13 21:04:27', '2017-09-13 21:04:27', NULL),
(63, 0, 'Waterpump Electronic & Parts', 'waterpump-electronic-parts', '', '2017-09-13 21:04:45', '2017-09-13 21:04:45', NULL),
(64, 0, 'Waterpumps', 'waterpumps', '', '2017-09-13 21:04:54', '2017-09-13 21:04:54', NULL),
(65, 36, 'Motorcycles', 'motorcycles', '', '2017-09-13 22:46:23', '2017-09-13 22:46:23', NULL),
(66, 37, 'Brush Cutter', 'brush-cutter', '', '2017-09-13 22:46:53', '2017-09-13 22:46:53', NULL),
(67, 37, 'Lawn Mower', 'lawn-mower', '', '2017-09-13 22:47:13', '2017-09-13 22:47:13', NULL),
(68, 37, 'Parts', 'parts', '', '2017-09-13 22:47:25', '2017-09-13 22:47:25', NULL),
(69, 38, 'Chains', 'chains', '', '2017-09-13 22:49:03', '2017-09-13 22:49:03', NULL),
(70, 38, 'Sprocket', 'sprocket', '', '2017-09-13 22:49:14', '2017-09-13 22:49:14', NULL),
(71, 38, 'Bearings', 'bearings', '', '2017-09-13 22:49:27', '2017-09-13 22:49:27', NULL),
(72, 38, 'Belt', 'belt', '', '2017-09-13 22:49:37', '2017-09-13 22:49:37', NULL),
(73, 38, 'V-Belt', 'vbelt', '', '2017-09-13 22:49:49', '2017-09-13 22:49:59', NULL),
(74, 39, 'High Pressure Cleaning', 'high-pressure-cleaning', '', '2017-09-13 22:50:23', '2017-09-13 22:50:23', NULL),
(75, 39, 'Accesories', 'accesories', '', '2017-09-13 22:50:32', '2017-09-13 22:50:32', NULL),
(76, 39, 'Parts', 'parts', '', '2017-09-13 22:50:42', '2017-09-13 22:50:42', NULL),
(77, 40, 'Unit', 'unit', '', '2017-09-13 22:50:57', '2017-09-13 22:50:57', NULL),
(78, 40, 'Parts', 'parts', '', '2017-09-13 22:51:04', '2017-09-13 22:51:04', NULL),
(79, 40, 'Chainsaw', 'chainsaw', '', '2017-09-13 22:51:12', '2017-09-13 22:51:12', NULL),
(80, 41, 'Parts', 'parts', '', '2017-09-13 22:51:23', '2017-09-13 22:51:23', NULL),
(81, 42, 'Tapping Machine', 'tapping-machine', '', '2017-09-13 22:51:40', '2017-09-13 22:51:40', NULL),
(82, 42, 'Hand Tap', 'hand-tap', '', '2017-09-13 22:52:03', '2017-09-13 22:52:03', NULL),
(83, 42, 'Drill', 'drill', '', '2017-09-13 22:52:52', '2017-09-13 22:52:52', NULL),
(84, 42, 'Dies', 'dies', '', '2017-09-13 22:53:54', '2017-09-13 22:53:54', NULL),
(85, 43, 'Unit', 'unit', '', '2017-09-13 22:54:06', '2017-09-13 22:54:06', NULL),
(86, 43, 'Parts', 'parts', '', '2017-09-13 22:54:14', '2017-09-13 22:54:14', NULL),
(87, 46, 'Unit', 'unit', '', '2017-09-13 22:54:39', '2017-09-13 22:54:39', NULL),
(88, 46, 'Parts', 'parts', '', '2017-09-13 22:54:45', '2017-09-13 22:54:45', NULL),
(89, 46, 'Accesories', 'accesories', '', '2017-09-13 22:54:53', '2017-09-13 22:54:53', NULL),
(90, 47, 'Chain Block', 'chain-block', '', '2017-09-13 22:55:56', '2017-09-13 22:55:56', NULL),
(91, 47, 'Lever Block', 'lever-block', '', '2017-09-13 22:56:08', '2017-09-13 22:56:08', NULL),
(92, 47, 'Electric Hoist', 'electric-hoist', '', '2017-09-13 22:56:19', '2017-09-13 22:56:19', NULL),
(93, 47, 'Accesories', 'accesories', '', '2017-09-13 22:56:27', '2017-09-13 22:56:27', NULL),
(94, 49, 'Unit', 'unit', '', '2017-09-13 22:57:00', '2017-09-13 22:57:00', NULL),
(95, 49, 'Parts', 'parts', '', '2017-09-13 22:57:14', '2017-09-13 22:57:14', NULL),
(96, 51, 'Agriculture', 'agriculture', '', '2017-09-13 22:58:02', '2017-09-13 22:58:02', NULL),
(97, 51, 'Industrial', 'industrial', '', '2017-09-13 22:58:14', '2017-09-13 22:58:14', NULL),
(98, 51, 'Parts', 'parts', '', '2017-09-13 22:58:22', '2017-09-13 22:58:22', NULL),
(99, 53, 'Oils', 'oils', '', '2017-09-13 22:58:41', '2017-09-13 22:58:41', NULL),
(100, 53, 'Grease', 'grease', '', '2017-09-13 22:58:50', '2017-09-13 22:58:50', NULL),
(101, 53, 'Other', 'other', '', '2017-09-13 22:58:58', '2017-09-13 22:58:58', NULL),
(102, 55, 'Power Sprayer', 'power-sprayer', '', '2017-09-13 22:59:12', '2017-09-13 22:59:12', NULL),
(103, 55, 'Knapsack', 'knapsack', '', '2017-09-13 22:59:20', '2017-09-13 22:59:20', NULL),
(104, 55, 'Electric Knapsack', 'electric-knapsack', '', '2017-09-13 22:59:31', '2017-09-13 22:59:31', NULL),
(105, 55, 'Parts', 'parts', '', '2017-09-13 22:59:41', '2017-09-13 22:59:41', NULL),
(106, 57, 'Powertools', 'powertools', '', '2017-09-13 22:59:59', '2017-09-13 22:59:59', NULL),
(107, 57, 'Accesories', 'accesories', '', '2017-09-13 23:00:07', '2017-09-13 23:00:07', NULL),
(108, 57, 'Parts', 'parts', '', '2017-09-13 23:00:24', '2017-09-13 23:00:24', NULL),
(109, 0, 'Seals', 'seals', '', '2017-09-13 23:00:30', '2017-09-13 23:00:49', '2017-09-13 23:00:49'),
(110, 59, 'Seals', 'seals', '', '2017-09-13 23:00:30', '2017-09-13 23:02:01', NULL),
(111, 59, 'Packings', 'packings', '', '2017-09-13 23:02:09', '2017-09-13 23:02:09', NULL),
(112, 59, 'Castor', 'castor', '', '2017-09-13 23:06:52', '2017-09-13 23:06:52', NULL),
(113, 59, 'Other', 'other', '', '2017-09-13 23:07:01', '2017-09-13 23:07:01', NULL),
(114, 60, 'Unit', 'unit', '', '2017-09-13 23:07:15', '2017-09-13 23:07:15', NULL),
(115, 60, 'Parts', 'parts', '', '2017-09-13 23:07:26', '2017-09-13 23:07:26', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `subject` varchar(20) DEFAULT NULL,
  `message` text NOT NULL,
  `details` text,
  `type` varchar(50) DEFAULT 'contact',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `message`, `details`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Coba', 'admin@admin.com', 'Ask Inquiry', '[ASK] Barang 1\r\nhttp://localhost/makmur/a', '{"ip":"::1"}', 'contact', '2017-10-23 00:24:30', '2017-10-23 00:24:30', NULL),
(2, 'afsdafdsfsdaf', 'admin@admin.com', 'fsafsdafsdafsda', 'fasdfdsfsdfsda', '{"ip":"::1"}', 'contact', '2017-10-23 00:25:29', '2017-10-23 00:25:29', NULL),
(3, 'afsdafdsfsdaf', 'admin@admin.com', 'fsafsdafsdafsda', 'fasdfdsfsdfsda', '{"ip":"::1"}', 'contact', '2017-10-23 00:26:53', '2017-10-23 00:26:53', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `special_offer` tinyint(1) DEFAULT NULL,
  `recom` tinyint(1) DEFAULT NULL,
  `best_selling` tinyint(1) DEFAULT NULL,
  `name` varchar(225) NOT NULL,
  `slug` text NOT NULL,
  `category` int(11) NOT NULL,
  `brand` int(11) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0',
  `show_price` tinyint(1) NOT NULL DEFAULT '0',
  `pictures` text NOT NULL,
  `brosur` text NOT NULL,
  `price` int(11) NOT NULL,
  `review` tinyint(1) NOT NULL DEFAULT '1',
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `items`
--

INSERT INTO `items` (`id`, `special_offer`, `recom`, `best_selling`, `name`, `slug`, `category`, `brand`, `stock`, `show_price`, `pictures`, `brosur`, `price`, `review`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 0, 0, 1, 'Barang 3', 'ab', 5, 1, 0, 1, '["http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/KYODO-NFM-130C.png","http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/KYODO-NFM-130C.png","http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/KYODO-NFM-130C.png"]', '"\\/media\\/files\\/shares\\/Generator_Cummins_Series.pdf"', 300000, 1, '<table>\r\n<tbody>\r\n<tr>\r\n<td>Model</td>\r\n<td>XXXXXXX</td>\r\n</tr>\r\n<tr>\r\n<td>Number Manufacter</td>\r\n<td>XXXXXXX</td>\r\n</tr>\r\n<tr>\r\n<td>Type</td>\r\n<td>XXXXXXX</td>\r\n</tr>\r\n</tbody>\r\n</table>', '2017-10-09 05:58:14', '2017-10-23 12:14:01', NULL),
(3, 0, 1, 0, 'Barang 1', 'a', 3, 2, 0, 1, '["http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/Bird_Nest_Farm_Equipments.png","http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/KYODO-NFM-130C.png","http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/TASCO-MIST-15.png","http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/TASCO-PADDY-REAPER-CG430.png"]', '"\\/media\\/files\\/shares\\/Generator_Cummins_Series.pdf"', 100000, 5, '<p style="box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; color: #666666; font-size: 13px; font-family: Montserrat, sans-serif;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum.</p>\r\n<p style="box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; color: #666666; font-size: 13px; font-family: Montserrat, sans-serif;">Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis.</p>', '2017-10-09 11:25:49', '2017-10-23 14:19:43', NULL),
(4, 1, 0, 0, 'Barang 4', 'coba', 5, 3, 0, 1, '["http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/TASCO-PADDY-REAPER-CG430.png","http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/TASCO-PADDY-REAPER-CG430.png","http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/TASCO-PADDY-REAPER-CG430.png"]', '"\\/media\\/files\\/shares\\/Genset_Kubota.pdf"', 2000000, 4, '<p>fdafsd</p>', '2017-10-09 12:08:04', '2017-10-23 12:14:06', NULL),
(5, 1, 0, 0, 'Barang 2', 'a-1', 5, 4, 0, 1, '["http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/TASCO-MIST-15.png","http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/TASCO-MIST-15.png","http:\\/\\/localhost\\/makmur\\/media\\/images\\/shares\\/TASCO-MIST-15.png"]', '"\\/media\\/files\\/shares\\/Genset_Kubota.pdf"', 3000000, 2, '<p>a</p>', '2017-10-10 12:11:55', '2017-10-23 12:13:55', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `lang`
--

CREATE TABLE `lang` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `dir` varchar(5) NOT NULL,
  `flag` varchar(20) DEFAULT NULL,
  `namespace` varchar(5) NOT NULL,
  `default_lang` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lang`
--

INSERT INTO `lang` (`id`, `name`, `dir`, `flag`, `namespace`, `default_lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Indonesia', 'id', 'flag-id.png', 'id', 1, '2017-08-10 00:20:43', '2017-08-10 00:20:43', NULL),
(2, 'English', 'en', 'flag-english.jpg', 'en', 0, '2017-08-10 00:20:51', '2017-08-10 00:20:51', NULL),
(3, 'Malaysia', 'my', 'flag-my.png', 'my', 0, '2017-08-10 03:08:34', '2017-08-10 23:17:15', '2017-08-10 23:17:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `slug` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `thumbnail` text NOT NULL,
  `description` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `gen_id` varchar(20) NOT NULL,
  `lang` int(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `category_id`, `thumbnail`, `description`, `status`, `gen_id`, `lang`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cara Pemesanan', 'cara-pemesanan', 3, '/media/images/shares/article-2.jpg', '<p style="margin-bottom: 0.2in; line-height: 100%; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-variant: normal;"><span style="color: #242424;"><span style="font-family: FreeSans, sans-serif;"><span style="letter-spacing: normal;"><span style="font-style: normal;"><span style="font-weight: normal;"><strong>A.Memilih Produk</strong></span></span></span></span></span></span></span></p>\r\n<ol>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Anda dapat melihat - lihat produk yang kami tawarkan begitu anda masuk di&nbsp;<em>beranda</em>&nbsp;halaman web kami.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Jika anda ingin melihat lebih mendetail atau mempermudah anda dalam mencari produk yang anda inginkan anda bisa mencarinya di&nbsp;<em>katagori produk</em>&nbsp;yang ada di samping kiri layar web kami, atau anda bisa mencarinya dengan mengetikan nama merk, atau type dari produk yang anda cari pada kolom&nbsp;<em>cari produk</em>&nbsp;yang ada diatas kanan halaman website kami.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Anda dapat klik&nbsp;<em>detail</em>&nbsp;pada produk jika ingin mengetahui spesifikasi dari produk tersebut.</span></span></span></span></p>\r\n</li>\r\n</ol>\r\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="color: #242424;">&nbsp;</span></span></p>\r\n<p style="orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-variant: normal;"><span style="color: #242424;"><span style="font-family: FreeSans, sans-serif;"><span style="letter-spacing: normal;"><span style="font-style: normal;"><span style="font-weight: normal;"><strong>B.Transaksi Pembelian</strong></span></span></span></span></span></span></span></p>\r\n<ol>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Jika sudah menemukan produk yang anda cari anda bisa membeli nya dengan klik&nbsp;<em>beli</em>&nbsp;pada gambar produk yang anda ingin beli.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Masukan jumlah quantity yang dibeli, lalu jila ingin meneruskan membeli produk lainnya klik&nbsp;<em>lanjutkan belanja</em>, jika sudah selesai klik&nbsp;<em>selesai belanja</em>.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Isikan data lengkap anda sesuai dengan yang diminta oleh kami, jika sudah di isi secara benar lanjutkan dengan klik&nbsp;<em>proses</em>, hingga muncul Proses&nbsp;<em>Transaksi Selesai</em>.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Segera lakukan pembayaran via transfer sebelum 3 hari atau pesanan anda kami anggap batal.</span></span></span></span></p>\r\n</li>\r\n</ol>\r\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="color: #242424;">&nbsp;</span></span></p>\r\n<p style="orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-variant: normal;"><span style="color: #242424;"><span style="font-family: FreeSans, sans-serif;"><span style="letter-spacing: normal;"><span style="font-style: normal;"><span style="font-weight: normal;"><strong>C.Proses Pembayaran</strong></span></span></span></span></span></span></span></p>\r\n<ol>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Pembayaran dapat dilakukan dengan transfer via Bank Danamon atau BCA dengan nomor rekening yang tertera di website kami.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Jika telah melakukan pembayaran segera kirim bukti pembayaran anda kepada kami dan kami akan segera mengirim produk yang telah anda beli dari website kami.</span></span></span></span></p>\r\n</li>\r\n</ol>\r\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="color: #242424;">&nbsp;</span></span></p>\r\n<p style="orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-variant: normal;"><span style="color: #242424;"><span style="font-family: FreeSans, sans-serif;"><span style="letter-spacing: normal;"><span style="font-style: normal;"><span style="font-weight: normal;"><strong>D.Pengiriman dan Garansi</strong></span></span></span></span></span></span></span></p>\r\n<ol>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Pengiriman akan dilaksanakan segera setelah anda mengirimkan bukti pembayaran kepada kami.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Jika dalam waktu 1 minggu produk yang anda beli tidak kunjung datang anda dapat melaporkannya kepada pihak kami, apabila memang produk tersebut belum dikirim anda dapat meminta uang anda kembali.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Periksalah kembali produk saat telah anda terima pastikan tidak ada kerusakan ataupun cacad pada produk.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Masa garansi setiap produk yang kami tawarkan berbeda - beda pastikan anda memeriksa kembali garansi nya.</span></span></span></span></p>\r\n</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p style="margin-bottom: 0in; line-height: 100%;">&nbsp;</p>', 'publish', '59dee88c95b34', 1, '2017-10-11 20:59:08', '2017-10-11 21:06:30', NULL),
(2, 'Cara Pemesanan', 'cara-pemesanan-2', 3, '/media/images/shares/article-2.jpg', '<p style="margin-bottom: 0.2in; line-height: 100%; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-variant: normal;"><span style="color: #242424;"><span style="font-family: FreeSans, sans-serif;"><span style="letter-spacing: normal;"><span style="font-style: normal;"><span style="font-weight: normal;"><strong>A.Memilih Produk</strong></span></span></span></span></span></span></span></p>\r\n<ol>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Anda dapat melihat - lihat produk yang kami tawarkan begitu anda masuk di&nbsp;<em>beranda</em>&nbsp;halaman web kami.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Jika anda ingin melihat lebih mendetail atau mempermudah anda dalam mencari produk yang anda inginkan anda bisa mencarinya di&nbsp;<em>katagori produk</em>&nbsp;yang ada di samping kiri layar web kami, atau anda bisa mencarinya dengan mengetikan nama merk, atau type dari produk yang anda cari pada kolom&nbsp;<em>cari produk</em>&nbsp;yang ada diatas kanan halaman website kami.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Anda dapat klik&nbsp;<em>detail</em>&nbsp;pada produk jika ingin mengetahui spesifikasi dari produk tersebut.</span></span></span></span></p>\r\n</li>\r\n</ol>\r\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="color: #242424;">&nbsp;</span></span></p>\r\n<p style="orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-variant: normal;"><span style="color: #242424;"><span style="font-family: FreeSans, sans-serif;"><span style="letter-spacing: normal;"><span style="font-style: normal;"><span style="font-weight: normal;"><strong>B.Transaksi Pembelian</strong></span></span></span></span></span></span></span></p>\r\n<ol>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Jika sudah menemukan produk yang anda cari anda bisa membeli nya dengan klik&nbsp;<em>beli</em>&nbsp;pada gambar produk yang anda ingin beli.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Masukan jumlah quantity yang dibeli, lalu jila ingin meneruskan membeli produk lainnya klik&nbsp;<em>lanjutkan belanja</em>, jika sudah selesai klik&nbsp;<em>selesai belanja</em>.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Isikan data lengkap anda sesuai dengan yang diminta oleh kami, jika sudah di isi secara benar lanjutkan dengan klik&nbsp;<em>proses</em>, hingga muncul Proses&nbsp;<em>Transaksi Selesai</em>.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Segera lakukan pembayaran via transfer sebelum 3 hari atau pesanan anda kami anggap batal.</span></span></span></span></p>\r\n</li>\r\n</ol>\r\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="color: #242424;">&nbsp;</span></span></p>\r\n<p style="orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-variant: normal;"><span style="color: #242424;"><span style="font-family: FreeSans, sans-serif;"><span style="letter-spacing: normal;"><span style="font-style: normal;"><span style="font-weight: normal;"><strong>C.Proses Pembayaran</strong></span></span></span></span></span></span></span></p>\r\n<ol>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Pembayaran dapat dilakukan dengan transfer via Bank Danamon atau BCA dengan nomor rekening yang tertera di website kami.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Jika telah melakukan pembayaran segera kirim bukti pembayaran anda kepada kami dan kami akan segera mengirim produk yang telah anda beli dari website kami.</span></span></span></span></p>\r\n</li>\r\n</ol>\r\n<p style="margin-bottom: 0in; font-variant: normal; letter-spacing: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="color: #242424;">&nbsp;</span></span></p>\r\n<p style="orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-variant: normal;"><span style="color: #242424;"><span style="font-family: FreeSans, sans-serif;"><span style="letter-spacing: normal;"><span style="font-style: normal;"><span style="font-weight: normal;"><strong>D.Pengiriman dan Garansi</strong></span></span></span></span></span></span></span></p>\r\n<ol>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Pengiriman akan dilaksanakan segera setelah anda mengirimkan bukti pembayaran kepada kami.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Jika dalam waktu 1 minggu produk yang anda beli tidak kunjung datang anda dapat melaporkannya kepada pihak kami, apabila memang produk tersebut belum dikirim anda dapat meminta uang anda kembali.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Periksalah kembali produk saat telah anda terima pastikan tidak ada kerusakan ataupun cacad pada produk.</span></span></span></span></p>\r\n</li>\r\n<li>\r\n<p style="margin-bottom: 0in; border: none; padding: 0in; font-variant: normal; letter-spacing: normal; font-style: normal; font-weight: normal; orphans: 2; widows: 2;" align="justify"><span style="display: inline-block; border: none; padding: 0in;"><span style="font-family: FreeSans, sans-serif;"><span style="font-size: small;"><span style="color: #242424;">Masa garansi setiap produk yang kami tawarkan berbeda - beda pastikan anda memeriksa kembali garansi nya.</span></span></span></span></p>\r\n</li>\r\n</ol>\r\n<p>&nbsp;</p>\r\n<p style="margin-bottom: 0in; line-height: 100%;">&nbsp;</p>', 'publish', '59dee88c95b34', 2, '2017-10-11 20:59:08', '2017-10-11 21:03:02', NULL),
(4, 'Coba Post Baru', 'coba-post-baru-1', 3, '/media/images/shares/article-4.jpg', '<h3 style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 24px; font-weight: 400; margin-top: 45px; margin-bottom: 10px; color: #525252; font-family: ''Source Sans Pro'', sans-serif;">Loops</h3>\r\n<p style="box-sizing: border-box; line-height: 1.7; margin: 10px 0px 20px; font-size: 14.5px; color: #525252; font-family: ''Source Sans Pro'', sans-serif;">In addition to conditional statements, Blade provides simple directives for working with PHP''s loop structures. Again, each of these directives functions identically to their PHP counterparts:</p>\r\n<pre class=" language-php" style="box-sizing: border-box; overflow: auto; font-family: ''Operator Mono'', ''Fira Code'', Consolas, Monaco, ''Andale Mono'', monospace; font-size: 11px; word-break: normal; direction: ltr; text-shadow: #ffffff 0px 1px; line-height: 2; tab-size: 4; hyphens: none; margin-top: 10px; margin-bottom: 20px; background: rgba(238, 238, 238, 0.35); border-radius: 3px; padding: 10px; box-shadow: rgba(0, 0, 0, 0.125) 0px 1px 1px; vertical-align: middle;"><code class=" language-php" style="box-sizing: border-box; font-family: ''Operator Mono'', ''Fira Code'', Consolas, Monaco, ''Andale Mono'', monospace; word-spacing: normal; word-break: normal; direction: ltr; text-shadow: #ffffff 0px 1px; line-height: 2; tab-size: 4; hyphens: none; vertical-align: middle;">@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">for</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">(</span><span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$i</span> <span class="token operator" style="box-sizing: border-box; color: #555555;">=</span> <span class="token number" style="box-sizing: border-box; color: #da564a;">0</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">;</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$i</span> <span class="token operator" style="box-sizing: border-box; color: #555555;">&lt;</span> <span class="token number" style="box-sizing: border-box; color: #da564a;">10</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">;</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$i</span><span class="token operator" style="box-sizing: border-box; color: #555555;">++</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">)</span>\r\n    The current value is <span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$i</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span>\r\n@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">endfor</span>\r\n\r\n@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">foreach</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">(</span><span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$users</span> <span class="token keyword" style="box-sizing: border-box; color: #0077aa;">as</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$user</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">)</span>\r\n    <span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>This is user <span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$user</span><span class="token operator" style="box-sizing: border-box; color: #555555;">-</span><span class="token operator" style="box-sizing: border-box; color: #555555;">&gt;</span><span class="token property" style="box-sizing: border-box; color: #da564a;">id</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span><span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;/</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>\r\n@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">endforeach</span>\r\n\r\n@forelse <span class="token punctuation" style="box-sizing: border-box; color: #999999;">(</span><span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$users</span> <span class="token keyword" style="box-sizing: border-box; color: #0077aa;">as</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$user</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">)</span>\r\n    <span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;</span>li</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$user</span><span class="token operator" style="box-sizing: border-box; color: #555555;">-</span><span class="token operator" style="box-sizing: border-box; color: #555555;">&gt;</span><span class="token property" style="box-sizing: border-box; color: #da564a;">name</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span><span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;/</span>li</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>\r\n@empty\r\n    <span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>No users<span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;/</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>\r\n@endforelse\r\n\r\n@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">while</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">(</span><span class="token boolean" style="box-sizing: border-box; color: #da564a;">true</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">)</span>\r\n    <span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>I''m looping forever<span class="token punctuation" style="box-sizing: border-box; color: #999999;">.</span><span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;/</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>\r\n@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">endwhile</span></code></pre>', 'publish', '59df0a287752e', 1, '2017-10-11 23:22:32', '2017-10-11 23:22:32', NULL),
(5, 'Coba Post Baru', 'coba-post-baru-2', 3, '/media/images/shares/article-4.jpg', '<p>&nbsp;</p>\r\n<h3 style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 24px; font-weight: 400; margin-top: 45px; margin-bottom: 10px; color: #525252; font-family: ''Source Sans Pro'', sans-serif;">Loops</h3>\r\n<p style="box-sizing: border-box; line-height: 1.7; margin: 10px 0px 20px; font-size: 14.5px; color: #525252; font-family: ''Source Sans Pro'', sans-serif;">In addition to conditional statements, Blade provides simple directives for working with PHP''s loop structures. Again, each of these directives functions identically to their PHP counterparts:</p>\r\n<pre class=" language-php" style="box-sizing: border-box; overflow: auto; font-family: ''Operator Mono'', ''Fira Code'', Consolas, Monaco, ''Andale Mono'', monospace; font-size: 11px; word-break: normal; direction: ltr; text-shadow: #ffffff 0px 1px; line-height: 2; tab-size: 4; hyphens: none; margin-top: 10px; margin-bottom: 20px; background: rgba(238, 238, 238, 0.35); border-radius: 3px; padding: 10px; box-shadow: rgba(0, 0, 0, 0.125) 0px 1px 1px; vertical-align: middle;"><code class=" language-php" style="box-sizing: border-box; font-family: ''Operator Mono'', ''Fira Code'', Consolas, Monaco, ''Andale Mono'', monospace; word-spacing: normal; word-break: normal; direction: ltr; text-shadow: #ffffff 0px 1px; line-height: 2; tab-size: 4; hyphens: none; vertical-align: middle;">@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">for</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">(</span><span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$i</span> <span class="token operator" style="box-sizing: border-box; color: #555555;">=</span> <span class="token number" style="box-sizing: border-box; color: #da564a;">0</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">;</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$i</span> <span class="token operator" style="box-sizing: border-box; color: #555555;">&lt;</span> <span class="token number" style="box-sizing: border-box; color: #da564a;">10</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">;</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$i</span><span class="token operator" style="box-sizing: border-box; color: #555555;">++</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">)</span>\r\n    The current value is <span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$i</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span>\r\n@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">endfor</span>\r\n\r\n@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">foreach</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">(</span><span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$users</span> <span class="token keyword" style="box-sizing: border-box; color: #0077aa;">as</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$user</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">)</span>\r\n    <span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>This is user <span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$user</span><span class="token operator" style="box-sizing: border-box; color: #555555;">-</span><span class="token operator" style="box-sizing: border-box; color: #555555;">&gt;</span><span class="token property" style="box-sizing: border-box; color: #da564a;">id</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span><span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;/</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>\r\n@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">endforeach</span>\r\n\r\n@forelse <span class="token punctuation" style="box-sizing: border-box; color: #999999;">(</span><span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$users</span> <span class="token keyword" style="box-sizing: border-box; color: #0077aa;">as</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$user</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">)</span>\r\n    <span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;</span>li</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">{</span> <span class="token variable" style="box-sizing: border-box; color: #4ea1df;">$user</span><span class="token operator" style="box-sizing: border-box; color: #555555;">-</span><span class="token operator" style="box-sizing: border-box; color: #555555;">&gt;</span><span class="token property" style="box-sizing: border-box; color: #da564a;">name</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">}</span><span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;/</span>li</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>\r\n@empty\r\n    <span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>No users<span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;/</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>\r\n@endforelse\r\n\r\n@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">while</span> <span class="token punctuation" style="box-sizing: border-box; color: #999999;">(</span><span class="token boolean" style="box-sizing: border-box; color: #da564a;">true</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">)</span>\r\n    <span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>I''m looping forever<span class="token punctuation" style="box-sizing: border-box; color: #999999;">.</span><span class="token markup" style="box-sizing: border-box;"><span class="token tag" style="box-sizing: border-box; color: #da564a;"><span class="token tag" style="box-sizing: border-box;"><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&lt;/</span>p</span><span class="token punctuation" style="box-sizing: border-box; color: #999999;">&gt;</span></span></span>\r\n@<span class="token keyword" style="box-sizing: border-box; color: #0077aa;">endwhile</span></code></pre>', 'publish', '59df0a287752e', 2, '2017-10-11 23:22:32', '2017-10-11 23:22:32', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `namespace` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `type` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sections`
--

INSERT INTO `sections` (`id`, `namespace`, `display_name`, `content`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'about_us', 'About Us', '[{"name":"title_tab","display_name":"Title Tab","type":"text"},{"name":"title","display_name":"Title","type":"text"},{"name":"content","display_name":"Content","type":"textarea_rich"},{"name":"link","display_name":"Link","type":"text"},{"name":"button_text","display_name":"Button Text","type":"text"}]', 'single', '2017-08-11 00:01:27', '2017-08-11 00:01:27', NULL),
(2, 'section_news', 'Section News', '[{"name":"title","display_name":"Title","type":"text"}]', 'single', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `section_details`
--

CREATE TABLE `section_details` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `lang` int(2) NOT NULL,
  `gen_id` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `section_details`
--

INSERT INTO `section_details` (`id`, `section_id`, `content`, `lang`, `gen_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '{"title_tab":"About MD","title":"About Makmur Diesel","content":"<p><\\/p>\\r\\n<p style=\\"margin-bottom: 0in; line-height: 100%;\\"><span style=\\"font-family: ''DejaVu Sans Light'', sans-serif;\\">Berawal dari toko kecil yang didirikan pada tahun 1940 an di Karawang, Jawa Barat. Oleh Bapak Abdi Wijaya yang bergerak dalam perdagangan hasil bumi dah bahan-bahan makanan di Karawang, dengan seiring pesatnya pembangunan maka usaha mulai beralih ke penjualan material bahan bangunan dan onderdil (sparepart) kendaraan.&nbsp;<br \\/> <br \\/> Pada tahun 1970, anak dari Bapak Abdi Wijaya yang bernama Bapak Arifin memulai perkembangan usaha dalam bidang mekanisasi perlalatan pertanian dengan nama perusahaan Toko Makmur yang terletak di Jl. Tuparev No.233 Karawang. Menjalin kerjasama dengan merk-merk terkenal baik lokal maupun import seperti merk mesin Andoria, Kirloskar, Thames, PELITA, Mitsubishi, Quick, Agrindo, Yanmar dan Kubota.&nbsp;<br \\/> <br \\/> Pesatnya pemmbangunan khususnya didaerah Kabupaten Karawang pada tahun 1996, anak dari Bapak Arifin yang bernama Bapak Hendra Arifin mulai memperluas usaha dari kebutuhan peralatan pertanian menjadi kebutuhan peralatan kerja, powertools, kebutuuhan industri, kontraktor dan rumah tangga.&nbsp;<br \\/> <br \\/> Sehingga pada tahun 2000 nama perusahaan Toko Makmur berubah nama menjadi UD. Makmur Diesel, dan pada tanggal 19 Januari 2007 dibentuklah PT. Ekawira Sarana Perkasa sebagai perusahaan dagang yang merupakan badan usaha dari nama merk dagang Makmur Diesel.<\\/span><\\/p>","link":"about-makmur-diesel","button_text":"Selengkapnya"}', 1, '59de1156bdd13', '2017-10-11 05:40:54', '2017-10-11 05:40:54', NULL),
(2, 1, '{"title_tab":null,"title":null,"content":null,"link":"about-makmur-diesel","button_text":null}', 2, '59de1156bdd13', '2017-10-11 05:40:54', '2017-10-11 05:40:54', NULL),
(3, 2, '{"title":"Berita"}', 1, '59dee2737bd0b', '2017-10-11 20:33:07', '2017-10-11 20:33:07', NULL),
(4, 2, '{"title":null}', 2, '59dee2737bd0b', '2017-10-11 20:33:07', '2017-10-11 20:33:07', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` int(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `value` text,
  `details` text NOT NULL,
  `setting_group` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `name`, `display_name`, `value`, `details`, `setting_group`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'site_name', 'Site Name', 'Makmur Diesel', '{"type":"text"}', 'general', '2017-03-10 00:33:38', '2017-10-11 20:03:03', NULL),
(2, 'site_logo', 'Logo', '/path/to/your/logo.png', '{"type":"files_images"}', 'general', '2017-03-10 01:07:46', '2017-10-11 20:03:03', NULL),
(3, 'email', 'Email', 'admin@makmurdiesel.com', '{"type":"text"}', 'contact', '2017-03-10 01:11:26', '2017-10-11 20:03:03', NULL),
(4, 'address', 'Address', 'Jl.Tuparev No.233 Karawang 41312', '{"type":"textarea"}', 'contact', '2017-03-10 03:16:36', '2017-10-11 20:03:03', NULL),
(5, 'phone', 'Phone', '(0267) 402877', '{"type":"text"}', 'contact', '2017-03-10 03:17:04', '2017-10-11 20:03:03', NULL),
(6, 'phone_2', 'Phone 2', '(0267) 8453112', '{"type":"text"}', 'contact', '2017-03-13 00:55:10', '2017-10-11 20:03:03', NULL),
(7, 'post_per_page', 'Post Per Page', '8', '{"type":"text"}', 'reading', '2017-08-11 03:12:25', '2017-10-11 20:03:03', NULL),
(8, 'site_logo_mobile', 'Site Logo Mobile', '/path/to/mobile/logo.png', '{"type":"files_images"}', 'general', '2017-08-11 03:55:00', '2017-10-11 20:03:03', NULL),
(9, 'email_alias_from', 'Email Alias From', 'your@email.com', '{"type":"text"}', 'mailable', '2017-08-16 03:57:04', '2017-10-11 20:03:03', NULL),
(10, 'email_alias_name', 'Email Alias Name', 'Your Email Name', '{"type":"text"}', 'mailable', '2017-08-16 03:57:22', '2017-10-11 20:03:03', NULL),
(11, 'email_to', 'Email To', 'your@email-again.com', '{"type":"text"}', 'mailable', '2017-08-16 03:57:37', '2017-10-11 20:03:03', NULL),
(13, 'about_picture', 'Photo About', '/media/images/shares/article-4.jpg', '{"type":"files_images"}', 'general', '2017-10-11 04:43:18', '2017-10-11 20:03:03', NULL),
(14, 'visi', 'Visi', 'Menjadi perusahaaan dagang dan distributor bertaraf international dan kokoh dengan pemanfaatan teknologi yang ramah bagi lingkungan.', '{"type":"textarea"}', 'general', '2017-10-11 04:48:03', '2017-10-11 20:03:03', NULL),
(15, 'misi', 'Misi', '<p>-Menjadi perusahaan yang bermanfaat bagi karyawan, masyarakat dan negara</p>\r\n<p>-Penyedia berbagai macam barang yang dibutuhkan dari berbagai segmen usaha <br /><br />-Menyediakan barang-barang dengan kualitas terbaik -Pendistribusian barang yang tepat waktu</p>\r\n<p>-Memprioritaskan kepuasan konsumen</p>\r\n<p>-Bekerja sama dan interaksi positif bagi kemajuan perusahaan</p>\r\n<p>-Menjadi mitra usaha terbaik bagi konsumen</p>', '{"type":"textarea_rich"}', 'general', '2017-10-11 04:48:14', '2017-10-11 20:03:03', NULL),
(16, 'visi_misi', 'Visi dan Misi', 'Dalam menjalani segala usaha untuk perusahaa, kami selalu berpegang erat dan berpedoman pada Visi dan Misi perusahaan agar dapat tercapai hasil yang maksimal dan segala usaha kami bermanfaat bagi seluruh patner dan konsumen PT. Ekawira Sarana Perkasa', '{"type":"textarea"}', 'general', '2017-10-11 04:48:29', '2017-10-11 20:03:03', NULL),
(17, 'kebijakan_mutu_perusahaan', 'Kebijakan Mutu Perusahaan', '<p><span style="color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;">Menjadi distributor alat teknik terkemuka dipasar nasional dan internasional melalui inovasi dan perbaikan terus menerus dengan komitmen total berorientasi kepada kepuasan pelanggan.&nbsp;</span><br style="padding: 0px; margin: 0px; color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;" /><span style="color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;">Agar dapat terwujudnya kebijakan mutu tersebut diatas maka PT. Ekawira Sarana Perkasa senantiasa berupaya keras dan berkomitmen tinggi untuk :&nbsp;</span><br style="padding: 0px; margin: 0px; color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;" /><span style="color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;">-Memberikan kepuasan kepada pelanggan</span><br style="padding: 0px; margin: 0px; color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;" /><span style="color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;">-Menjaga pertumbuhan dan perkembangan perusahaan</span><br style="padding: 0px; margin: 0px; color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;" /><span style="color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;">-Memiliki keunggulan bisnis dengan :&nbsp;</span><br style="padding: 0px; margin: 0px; color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;" /><br style="padding: 0px; margin: 0px; color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;" /><span style="color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;">~Menyediakan berbagai produk yang berkualitas dan inovatif dengan harga yang bersaing</span><br style="padding: 0px; margin: 0px; color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;" /><span style="color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;">~Membangun jaringan distribusi yanng luas dan kuat</span><br style="padding: 0px; margin: 0px; color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;" /><span style="color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;">~Membentuk sumber daya manusia yang berkompetensi tinggi</span><br style="padding: 0px; margin: 0px; color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;" /><span style="color: #242424; font-family: ''Segoe UI''; font-size: 13.3333px; text-align: justify;">~Melakukan perbaikan yang berkesinambungan dengan cepat</span></p>', '{"type":"textarea_rich"}', 'general', '2017-10-11 04:59:13', '2017-10-11 20:03:03', NULL),
(18, 'welcome_message', 'Welcome Message', 'Selamat Datang', '{"type":"text"}', 'general', '2017-10-11 20:02:56', '2017-10-11 20:03:03', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `gambar` text NOT NULL,
  `link` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sliders`
--

INSERT INTO `sliders` (`id`, `gambar`, `link`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '/media/images/shares/slide-1.jpg', 'http://localhost/makmur/a', '2017-10-23 08:01:32', '2017-10-23 08:03:25', NULL),
(2, '/media/images/shares/slide-2.jpg', 'http://localhost/makmur/a-1', '2017-10-23 08:01:44', '2017-10-23 08:03:42', NULL),
(3, '/media/images/shares/slide-3.jpg', 'http://localhost/makmur/ab', '2017-10-23 08:02:01', '2017-10-23 08:03:49', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$HKXEIvihSfHyHTYjUaHUou0UjBiUPpF2KIcqoB.1IM46ypiWN.4W6', 'g18FJBzq8o4Zp8JxIwwOcfbu4WKyjeULrlkPtqsIR43IKcj6Xnb2LoG6tK0c', '2017-03-01 20:55:49', '2017-03-09 01:41:08', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lang`
--
ALTER TABLE `lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section_details`
--
ALTER TABLE `section_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `lang`
--
ALTER TABLE `lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `section_details`
--
ALTER TABLE `section_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
