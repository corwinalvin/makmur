<?php

namespace App\Repositories;

use App\Models\Sliders;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SlidersRepository
 * @package App\Repositories
 * @version October 23, 2017, 7:51 am UTC
 *
 * @method Sliders findWithoutFail($id, $columns = ['*'])
 * @method Sliders find($id, $columns = ['*'])
 * @method Sliders first($columns = ['*'])
*/
class SlidersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'gambar',
        'link'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sliders::class;
    }
}
