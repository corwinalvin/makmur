<?php

namespace App\Repositories;

use App\Models\Brands;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BrandsRepository
 * @package App\Repositories
 * @version October 10, 2017, 5:21 am UTC
 *
 * @method Brands findWithoutFail($id, $columns = ['*'])
 * @method Brands find($id, $columns = ['*'])
 * @method Brands first($columns = ['*'])
*/
class BrandsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'brand'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Brands::class;
    }
}
