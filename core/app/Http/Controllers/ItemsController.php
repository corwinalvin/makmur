<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateItemsRequest;
use App\Http\Requests\UpdateItemsRequest;
use App\Repositories\ItemsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Categories;
use App\Models\Brands;

class ItemsController extends AppBaseController
{
    /** @var  ItemsRepository */
    private $itemsRepository;

    public function __construct(ItemsRepository $itemsRepo)
    {
        $this->itemsRepository = $itemsRepo;
    }

    /**
     * Display a listing of the Items.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->itemsRepository->pushCriteria(new RequestCriteria($request));
        $items = $this->itemsRepository->all();

        return view('items.index')
            ->with('items', $items);
    }

    /**
     * Show the form for creating a new Items.
     *
     * @return Response
     */
    public function create()
    {
        $categori = Categories::all();
        $categories = array();
        foreach ($categori as $value) {
            $categories[$value->id] = $value->name;
        }
        $brand = Brands::all();
        $brands = array();
        foreach ($brand as $value) {
            $brands[$value->id] = $value->brand;
        }
        return view('items.create', compact('categories', 'brands'));
    }

    /**
     * Store a newly created Items in storage.
     *
     * @param CreateItemsRequest $request
     *
     * @return Response
     */
    public function store(CreateItemsRequest $request)
    {
        $input = $request->all();
        $input['pictures'] = (isset($input['pictures']) ? json_encode($input['pictures']) : json_encode(['noproduct.png']));
        $input['brosur'] = (isset($input['brosur']) ? json_encode($input['brosur']) : json_encode(['noproduct.png']));
        $items = $this->itemsRepository->create($input);
        Flash::success('Items saved successfully.');

        return redirect(route('items.index'));
    }

    /**
     * Display the specified Items.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $items = $this->itemsRepository->findWithoutFail($id);

        if (empty($items)) {
            Flash::error('Items not found');

            return redirect(route('items.index'));
        }

        return view('items.show')->with('items', $items);
    }

    /**
     * Show the form for editing the specified Items.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $items = $this->itemsRepository->findWithoutFail($id);
        $categori = Categories::all();
        $categories = array();
        foreach ($categori as $value) {
            $categories[$value->id] = $value->name;
        }
        $brand = Brands::all();
        $brands = array();
        foreach ($brand as $value) {
            $brands[$value->id] = $value->brand;
        }
        if (empty($items)) {
            Flash::error('Items not found');

            return redirect(route('items.index'));
        }

        return view('items.edit', compact('categories', 'brands'))->with('items', $items);
    }

    /**
     * Update the specified Items in storage.
     *
     * @param  int              $id
     * @param UpdateItemsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateItemsRequest $request)
    {
        $items = $this->itemsRepository->findWithoutFail($id);

        if (empty($items)) {
            Flash::error('Items not found');

            return redirect(route('items.index'));
        }

        $input = $request->all();
        if (!isset($input['show_price'])) {
            $input['show_price'] = 0;
        }
        $input['brosur'] = (isset($input['brosur']) ? json_encode($input['brosur']) : json_encode(['noproduct.png']));
        $input['pictures'] = (isset($input['pictures']) ? json_encode($input['pictures']) : json_encode(['noproduct.png']));
        $items = $this->itemsRepository->update($input, $id);

        Flash::success('Items updated successfully.');

        return redirect(route('items.index'));
    }

    /**
     * Remove the specified Items from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $items = $this->itemsRepository->findWithoutFail($id);

        if (empty($items)) {
            Flash::error('Items not found');

            return redirect(route('items.index'));
        }

        $this->itemsRepository->delete($id);

        Flash::success('Items deleted successfully.');

        return redirect(route('items.index'));
    }
}
