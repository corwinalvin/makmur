<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests\CreateContactRequest;
use App\Http\Controllers\Controller;
use App\Models\Posts;
use App\Models\Contact;
use App\Models\Items;
use App\Models\Categories;
use App\Models\Brands;
use App;
use DB;
use SEOMeta;
use SEO;

class HomeFrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        SEOMeta::setTitle(setting('seo_title'));
        SEOMeta::setDescription(setting('seo_description'));
        SEOMeta::addKeyword(setting('seo_keyword'));

        $so = Items::where('special_offer', 1)->get();
        $bs = Items::where('best_selling', 1)->get();
        return view('frontend.pages.home', compact('so', 'bs'));
    }

    public function postDetail($id) {
        
        $post = Posts::with('category')->find($id);
        $posts = Posts::whereNull('deleted_at')->where('id', '<>', $id)->whereLang(getLangPlease())->limit(10)->get();
        if(!count($post)) {
            return 'failed';
        }
        SEO::setTitle($post->title);
        SEO::setDescription(strip_tags(str_limit($post->description, 1500)));
        SEOMeta::addKeyword($post->slug);
        return view('frontend.pages.posts',compact('post', 'posts'));
    }

    public function howOrder($value='')
    {
        $artikel = Posts::select('id')->where('slug', 'like', 'cara-pemesanan')->whereLang(getLangPlease())->first();
        SEO::setTitle($artikel->title);
        SEO::setDescription(strip_tags(str_limit($artikel->description, 1500)));
        SEOMeta::addKeyword($artikel->slug);
        return $this->postDetail($artikel->id);
    }

    public function faq() {
        SEO::setTitle('FAQ - Makmur Diesel');
        SEO::setDescription('FAQ - Makmur Diesel');
        SEOMeta::addKeyword('FAQ - Makmur Diesel');
        return view('frontend.pages.faqs');
    }
 
    public function brands() {
        SEO::setTitle('Brand - Makmur Diesel');
        SEO::setDescription('Brand - Makmur Diesel');
        SEOMeta::addKeyword('Brand - Makmur Diesel');
        $manufacters = Brands::get();
        return view('frontend.pages.manufacters', compact('manufacters'));
    }

    public function about() {
        SEO::setTitle('About - Makmur Diesel');
        SEO::setDescription('About - Makmur Diesel');
        SEOMeta::addKeyword('About - Makmur Diesel');
        return view('frontend.pages.about');
    }

    public function product($value='', $id='') {
        $limit = (isset(request()->limit) ? request()->limit : 12);
        $sort = (isset(request()->sort_by) ? request()->sort_by : 'name' );
        $sortP = (isset(request()->order_by) ? request()->order_by : 'asc');

        $item = Items::where('slug', $value)->first();
        $manufacters = Brands::get();
        if ($item != null) {
            $rItems = Items::where('category', $item->category)->where('id', '<>', $item->id)->get();
            SEO::setTitle($item->name);
            SEO::setDescription(strip_tags($item->description));
            SEOMeta::addKeyword($item->name);
            return view('frontend.pages.single', compact('item', 'rItems', 'manufacters'));
        }else if ($id != ""){
            $items = Items::whereHas('categories', function($query) use ($id){
                $query->where('slug', $id);
            })->orderBy($sort, $sortP)->paginate($limit);
            $items->appends(['limit' => $limit, 'sort_by' => $sort, 'order_by' => $sortP]);
            SEO::setTitle(ucwords(strip_tags($id)));
            SEO::setDescription(ucwords(strip_tags($id)));
            SEOMeta::addKeyword(ucwords(strip_tags($id)));
            return view('frontend.pages.category', compact('items', 'manufacters'));
        }else{
            $cat = Categories::where('slug', $value)->first();
            if ($cat != null) {
                $items = Items::whereHas('categories', function($query) use ($value, $cat){
                    $query->where('slug', $value)->orWhere('parent_id', $cat->id);
                })->orderBy($sort, $sortP)->paginate($limit);
                $items->appends(['limit' => $limit, 'sort_by' => $sort, 'order_by' => $sortP]);
                SEO::setTitle($cat->name);
                SEO::setDescription(strip_tags($cat->description));
                SEOMeta::addKeyword($cat->title);
                return view('frontend.pages.category', compact('items', 'manufacters'));
            }
            $cek = Items::whereHas('brands', function($query) use ($value){
                    $query->where('brand', 'like' , '%'.$value.'%');
                })->first();
            if ($cek != null) {
                $items = Items::whereHas('brands', function($query) use ($value){
                            $query->where('brand', 'like' , '%'.$value.'%');
                        })->paginate($limit);
                $items->appends(['limit' => $limit, 'sort_by' => $sort, 'order_by' => $sortP]);
                SEO::setTitle($cek->title);
                SEO::setDescription(strip_tags($cek->description));
                SEOMeta::addKeyword($cek->title);
                return view('frontend.pages.category', compact('items', 'manufacters'));
            }
            return abort(404);
        }
    }

    public function contact() {
        SEO::setTitle('Contact - Makmur Diesel');
        SEO::setDescription('Contact - Makmur Diesel');
        SEOMeta::addKeyword('Contact - Makmur Diesel');
        return view('frontend.pages.contact');
    }

    public function lang($id) {
        session(['my_locale' => $id]);
        return redirect()->back();
    }

    public function search(Request $request) {
        $category = ($request->category == "") ? "all" : "all";
        $limit = (isset(request()->limit) ? request()->limit : 12);
        $word = $request->word;
        $manufacters = Brands::get();
        if ($category == "all") {
            $items = Items::where('name', 'like', '%'.$word.'%')->orWhere(function($query) use ($word){
                $query->whereHas('brands', function($que) use ($word){
                    $que->where('brand', $word);
                });
            })->paginate($limit);
            $items->appends(['category' => $category, 'word' => $word]);
        }else{
            $cat = Categories::where('slug', $category)->first();
            $items = Items::where('name', 'like', '%'.$word.'%')->whereHas('categories', function($query) use ($category, $cat){
                $query->where('slug', $category)->orWhere('parent_id', $cat->id);
            })->paginate($limi);
            $items->appends(['category' => $category, 'word' => $word]);
        }
        SEO::setTitle('Makmur Diesel - '.$word);
        SEO::setDescription('Makmur Diesel - '.$word);
        SEOMeta::addKeyword('Makmur Diesel - '.$word);
        return view('frontend.pages.category', compact('items', 'manufacters'));
    }

    public function downloadPDF($id)
    {
        $item = Items::find($id);
        $file = storage_path() . json_decode($item->brosur);
        $headers = [
          'Content-Type' => 'application/pdf',
        ];
        return response()->download($file, $item->name.' '.$item->brands->brand ,$headers);
    }
}
