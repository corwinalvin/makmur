<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class Items
 * @package App\Models
 * @version October 9, 2017, 3:07 am UTC
 */
class Items extends Model
{
    use SoftDeletes;
    use Sluggable;

    public $table = 'items';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'category',
        'brand',
        'pictures',
        'price',
        'description',
        'brosur',
        'show_price',
        'stock',
        'review',
        'special_offer', 
        'recom',
        'best_selling'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'category' => 'integer',
        'brand' => 'integer',
        'pictures' => 'string',
        'price' => 'integer',
        'description' => 'string',
        'brosur' => 'string',
        'show_price' => 'integer',
        'stock' => 'integer',
        'review' => 'integer',
        'special_offer' => 'integer', 
        'recom' => 'integer',
        'best_selling' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function categories() {
        return $this->belongsTo('App\Models\Categories', 'category');
    }

    public function brands()
    {
        return $this->belongsTo('App\Models\Brands', 'brand');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    
}
