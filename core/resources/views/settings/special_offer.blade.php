@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Special Offer Items</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('settings.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-responsive" id="settings-table">
                    <thead>
                        <th>Name</th>
                        <th>Display Name</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach ($items as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->categories->name}}</td>
                        </tr>
                    @endforeach                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

