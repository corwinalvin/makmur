<style>
.image-chooser {
    margin-top: 20px;
}
.image-chooser .item {
    display: inline-block;
    float: left;
    width: 100px;
    height: 100px;
    border: 1px solid #ddd;
    background-color: #f2f2f2;
    text-align: center;
    cursor: pointer;
    transition: all .5s;
    margin-right: 10px;
    overflow: hidden;
    position: relative;
    margin-bottom: 10px;
}

.image-chooser .item img {
    width: 100%;
    display: inline-block;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
}

.image-chooser .item:hover {
    opacity: .8;
}

.image-chooser .item .ion {
    margin-top: 22px;
    font-size: 30px;
    display: block;
}

.image-chooser .item .close {
    position: absolute;
    top: 0;
    right: 0;
    background-color: #fff;
    padding: 5px;
    opacity: 1;
}

.image-chooser .item .close .ion {
    margin: 0;
    font-size: 16px;
}
</style>
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group col-sm-3">
    {!! Form::label('stock', 'Stock:') !!}
     {!! Form::number('stock', null, ['class' => 'form-control', 'required', 'onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57']) !!}
</div>

<!-- Review Field -->
<div class="form-group col-sm-3">
    {!! Form::label('review', 'Review:') !!}
    {!! Form::number('review', null, ['class' => 'form-control', 'required','max' => '5', 'onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57']) !!}
</div>

<!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category', 'Category:') !!}
    <!-- {!! Form::select('category', $categories, old('category'), ['class' => 'form-control js-example-basic-single','id'=>'category', 'required']) !!} -->
    <select name="category" id="category" class="form-control js-example-basic-single" required="">
     @foreach(allCategories() as $category)
      @if($category->parent_id == 0)
        @if(!empty(json_decode(getCategory($category->id))))
        <option disabled="">{{$category->name}}</option>
        <?php foreach(getCategory($category->id) as $sub) : ?> 
            <option value="{{$sub->id}}"  {{(isset($items->category) && $items->category == $sub->id) ? 'selected=selected' : '' }}>&nbsp;&nbsp;&nbsp; {{$sub->name}} </option>';
        <?php endforeach ?>
        @else
        <option value="{{$category->id}}" {{(isset($items->category) && $items->category == $category->id) ? 'selected=selected' : '' }}>{{$category->name}}</option>
        @endif
      @endif
    @endforeach
    </select>
</div>


<!-- Brand Field -->
<div class="form-group col-sm-6">
    {!! Form::label('brand', 'Brand:') !!}
    {!! Form::select('brand', $brands, old('brand'), ['class' => 'form-control js-example-basic-single','id'=>'category', 'required']) !!}
</div>

<!-- Pictures Field -->
<div class="form-group col-sm-12">
    {!! Form::label('pictures', 'Pictures:') !!}
    <div class="help-block">Anda dapat memilih lebih dari 1 gambar</div>
    <div id="selected_image" class="image-chooser">
        <div class="image-chooser-list"></div>
        <div class="toggle item">
            <i class="ion ion-plus"></i>
            Add Image
        </div>
    </div>
    <!-- <input type="file" name="pictures[]" multiple="" class="form-control"> -->
</div>


<!-- Deskripsi Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('desc', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control tinymce', 'required']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control currency', 'required', 'onkeypress' => 'return event.charCode >= 48 && event.charCode <= 57']) !!}
    <div class="checkbox">
    <label>
        {!!Form::checkbox('show_price', 1)!!}
        Munculkan harga di depan
    </label>
  </div>
</div>


<!-- Brosure Field -->
<div class="form-group col-sm-6">
    {!! Form::label('brochure', 'Brochure:') !!}
     <div class="input-group">
       <span class="input-group-btn">
         <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
           <i class="fa fa-file-o"></i> Choose
         </a>
       </span>
       @if(isset($items->brosur) && $items->brosur != "")
        <input id="thumbnail" class="form-control" type="text" value="{{json_decode($items->brosur)}}" name="brosur" required="">
       @else
        <input id="thumbnail" class="form-control" type="text" name="brosur" required="">
       @endif
     </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('items.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
var count = 0;
$(".image-chooser").each(function(){
    var $this = $(this);
    $(this).find(".toggle").click(function(){
        image_chooser_add($this);
    });

    @if(isset($items->pictures) && json_decode($items->pictures) != "")
    @foreach(json_decode($items->pictures) as $pic)
        image_chooser_add($this, '{{url($pic)}}');
    @endforeach
    @endif
});
$('#lfm').filemanager('file', {
    prefix: '{{url("/")}}/laravel-filemanager'
});
function image_chooser_add($this, item) {
    count ++;
    if(!item) {
        item = null;
    }
    element = '<div class="item ui-state-default" data-fm="true" data-input="picture'+count+'" data-preview="image-chooser-preview'+count+'"><img id="image-chooser-preview'+count+'" '+(item ? 'src="'+item+'"':'')+'><i class="ion ion-image"></i>Pick Image<input type="hidden" name="pictures[]" id="picture'+count+'" '+(item ? 'value="'+item+'"' : '')+'><div class="close" onclick="$(this).parent().remove()"><i class="ion ion-close"></i></div></div>';
    $this.find(".image-chooser-list").append(element);
    $('[data-fm="true"]').filemanager('image', {
        prefix: '{{url("/")}}/laravel-filemanager'
    });
}
</script>

@endsection