<style>
    td.rating{
        color: #FC0;
    }
</style>
<table class="table table-responsive" id="items-table">
    <thead>
        <th>Name</th>
        <th>Category</th>
        <th>Brand</th>
        <th>Price</th>
        <th>Review</th>
        <th>Stock</th>
        <th>Nm. of Image(s)</th>
        <th>Action</th>
    </thead>
    <tbody>
    @foreach($items as $items)
        <tr>
            <td>{!! $items->name !!}</td>
            <td>{!! getCategoryBySlug($items->category)[0]['parent']->name !!} - {!! getCategoryBySlug($items->category)[0]['cat']->name !!}</td>
            <td>{{$items->brands->brand}}</td>
            <td>IDR {!! number_format($items->price) !!}</td>
            <td class="rating">          
                @if(5-$items->review > 0)
                @for ($i = 0; $i < $items->review; $i++)
                  <i class="fa fa-star">  </i>
                @endfor
                @for ($i = 0; $i < 5-$items->review; $i++)
                  <i class="fa fa-star-o">  </i>
                @endfor
                @endif
            </td>
            <td>
                @if($items->stock > 0)
                    {{$items->stock}} Stock(s)
                @else
                    Kosong
                @endif
            </td>
            <td>
                <?php
                    $arr = json_decode($items->pictures, true);
                    $count = 0;
                    for ($i=0; $i < count($arr); $i++) { 
                        $count++;
                    }
                    echo $count;
                ?>
            </td>
            <td>
                {!! Form::open(['route' => ['items.destroy', $items->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('items.show', [$items->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('items.edit', [$items->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>