<!-- Id Field -->
<div class="form-group col-lg-6">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $items->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group col-lg-6">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $items->name !!}</p>
</div>

<!-- Category Field -->
<div class="form-group col-lg-6">
    {!! Form::label('category', 'Category:') !!}
    <p>{!! $items->categories->name !!}</p>
</div>

<!-- Price Field -->
<div class="form-group col-lg-6">
    {!! Form::label('price', 'Price:') !!}
    <p>IDR {!! number_format($items->price) !!}</p>
</div>
<!-- Pictures Field -->
<div class="form-group col-lg-12">
    {!! Form::label('pictures', 'Pictures:') !!}
    <p>
    @foreach (json_decode($items->pictures) as $pic)
       <img src="{{url($pic)}}" class="img" width="20%">
    @endforeach
    </p>
</div>

<div class="form-group col-lg-12">
    <label for="">Brochure</label>
    <p>
        <iframe src="{{url(json_decode($items->brosur))}}" frameborder="0" width="100%" height="500px"></iframe>
    </p>
</div>
