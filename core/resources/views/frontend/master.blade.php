<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
        {!! SEO::generate(true) !!}
		<title>@yield('title')</title>
		<link rel="icon" href="{{asset('images/fav-icon.png')}}" type="image/x-icon">
    <script src="{{url('scripts/front/jquery.min.js')}}"></script>
    <!-- <script src="{{url('scripts/front/jquery.turbolinks.min.js')}}"></script> -->
    <link rel="stylesheet" href="{{url('css/frontend/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/animate.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{url('css/frontend/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/pe-icon-7-stroke.min.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/owl.transitions.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/flexslider.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/revolution-slider.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/quick_view_popup.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/blog.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/shortcode.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/shortcodes/shortcodes.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/shortcodes/featured-box.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/shortcodes/pricing-table.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/shortcodes/tooltip.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/shortcodes/post.css')}}">
    <link rel="stylesheet" href="{{url('css/frontend/style.css')}}">
		<link rel="stylesheet" href="{{url('css/frontend/responsive.css')}}">
      <!--[if IE]>
        <link href="css/ie.css" media="screen, projection" rel="stylesheet" />
    <![endif]-->
	</head>
  @if(Request::is('/'))
  <body class="cms-index-index cms-home-page">
  @else
	<body class="product-page">
  @endif
		@include('frontend.parts.menu')
		<div id="page">
            @include('frontend.parts.header')
            @include('frontend.parts.navbar')
			      @yield('content')
			@include('frontend.parts.footer')
		</div>
    <script src="{{url('scripts/front/bootstrap.min.js')}}" ></script>
    <script src="{{url('scripts/front/owl.carousel.min.js')}}" ></script>
    <script src="{{url('scripts/front/mobile-menu.js')}}" ></script>
    <script src="{{url('scripts/front/cloud-zoom.js')}}" ></script>
    <script src="{{url('scripts/front/jquery.flexslider.js')}}" ></script>
    <script src="{{url('scripts/front/jquery-ui.js')}}" ></script>
    <script src="{{url('scripts/front/main.js')}}" ></script>
    <script>
      $(function() {
          // whenever we hover over a menu item that has a submenu
          $('li.parent').on('mouseover', function() {
            var $menuItem = $(this),
                $submenuWrapper = $('> .wrap-popup', $menuItem);
            
            // grab the menu item's position relative to its positioned parent
            var menuItemPos = $menuItem.position();
            
            // place the submenu in the correct position relevant to the menu item
            $submenuWrapper.css({
              top: menuItemPos.top,
              left: menuItemPos.left + Math.round($menuItem.outerWidth() * 1)
            });
          });
        });
      function imgError(image) {
          image.onerror = "";
          image.src = '{{url('images/baner_stai1.png')}}';
          return true;
      }
    </script>
	  @yield('js')
    @if(Request::is('/'))
    <script src="{{url('scripts/front/revolution-slider.js')}}" ></script>
    <script type='text/javascript'>
        jQuery(document).ready(function(){
            jQuery('#rev_slider_4').show().revolution({
                dottedOverlay: 'none',
                delay: 5000,
                startwidth: 865,
                startheight: 450,

                hideThumbs: 200,
                thumbWidth: 200,
                thumbHeight: 50,
                thumbAmount: 2,

                navigationType: 'thumb',
                navigationArrows: 'solo',
                navigationStyle: 'round',

                touchenabled: 'on',
                onHoverStop: 'on',
                
                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,
            
                spinner: 'spinner0',
                keyboardNavigation: 'off',

                navigationHAlign: 'center',
                navigationVAlign: 'bottom',
                navigationHOffset: 0,
                navigationVOffset: 20,

                soloArrowLeftHalign: 'left',
                soloArrowLeftValign: 'center',
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,

                soloArrowRightHalign: 'right',
                soloArrowRightValign: 'center',
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,

                shadow: 0,
                fullWidth: 'on',
                fullScreen: 'off',

                stopLoop: 'off',
                stopAfterLoops: -1,
                stopAtSlide: -1,

                shuffle: 'off',

                autoHeight: 'off',
                forceFullWidth: 'on',
                fullScreenAlignForce: 'off',
                minFullScreenHeight: 0,
                hideNavDelayOnMobile: 1500,
            
                hideThumbsOnMobile: 'off',
                hideBulletsOnMobile: 'off',
                hideArrowsOnMobile: 'off',
                hideThumbsUnderResolution: 0,
          

                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                fullScreenOffsetContainer: ''
            });
        });
        </script>
      @endif
      <!-- <script src="{{url('scripts/front/turbolink.js')}}" ></script> -->
      <!-- <script src="{{url('scripts/front/turbolink-build.js')}}" ></script> -->
  </body>
</html>