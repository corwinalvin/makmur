<nav>
    <div class="container">
      <div class="row">
        <div class="mm-toggle-wrap">
          <div class="mm-toggle"><i class="fa fa-align-justify"></i> </div>
          <span class="mm-label">All Categories</span> </div>
        <div class="col-md-3 col-sm-3 mega-container hidden-xs">
          <div class="navleft-container">
            <div class="mega-menu-title">
              <h3><span>All Categories</span></h3>
            </div>
            
            <!-- Shop by category -->
            <div class="mega-menu-category">
              <ul class="nav">
              @foreach(allCategories() as $category)
                @if($category->parent_id == 0)
                  @if(!empty(json_decode(getCategory($category->id))))
                  <li class="parent"><a href="{{route('frontend.product', ['id' => $category->slug])}}">{{$category->name}}</a>
                    <div class="wrap-popup column1">
                      <div class="popup">
                        <ul class="nav">
                        <?php 
                          foreach(getCategory($category->id) as $sub):
                              echo '<li><a href='.url($category->slug.'/'.$sub->slug).'><span>'.$sub->name.'</span></a></li>';
                          endforeach
                        ?>
                        </ul>
                      </div>
                    </div>
                  </li>
                  @else
                  <li class="nosub"><a href="{{url($category->slug)}}">{{$category->name}}</a></li>
                  @endif
                @endif
              @endforeach
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-9 col-sm-9 jtv-megamenu">
          <div class="mtmegamenu">
            <ul class="hidden-xs">
              <li class="mt-root">
                <div class="mt-root-item"><a href="{{route('frontend.product', ['id' => 'air-compressor-accesories-parts/unloading'])}}">
                  <div class="title title_font"><span class="title-text">Products</span></div>
                  </a>
                </div>
              </li>
              <li class="mt-root">
                <div class="mt-root-item"><a href="{{route('manufacters')}}">
                  <div class="title title_font"><span class="title-text">Brands</span></div>
                  </a>
                </div>
              </li>
              <li class="mt-root">
                <div class="mt-root-item"><a href="{{route('frontend.about')}}">
                  <div class="title title_font"><span class="title-text">About Us</span></div>
                  </a>
                </div>
              </li>
               <li class="mt-root">
                <div class="mt-root-item"><a href="{{route('frontend.contact')}}">
                  <div class="title title_font"><span class="title-text">Contact Us</span></div>
                  </a>
                </div>
              </li>
              <!-- <li class="mt-root">
                <div class="mt-root-item"><a href="#">
                  <div class="title title_font"><span class="title-text">Categories</span></div>
                  </a></div>
                <ul class="menu-items col-xs-12">
                  <li class="menu-item depth-1 menucol-1-3 ">
                    <div class="title title_font"> <a href="#">Fashion</a></div>
                    <ul class="submenu">
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Women</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Men</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Kids</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Clothings</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Shoes</a></div>
                      </li>
                    </ul>
                  </li>
                  <li class="menu-item depth-1 menucol-1-3 ">
                    <div class="title title_font"> <a href="#">Electronics </a></div>
                    <ul class="submenu">
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Mobiles</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Computers</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Headphones</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Laptops</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Appliances</a></div>
                      </li>
                    </ul>
                  </li>
                  <li class="menu-item depth-1 menucol-1-3 ">
                    <div class="title title_font"> <a href="#">Beauty & Health</a></div>
                    <ul class="submenu">
                      <li class="menu-item depth-2 category ">
                        <div class="title"> <a href="shop_grid.html">Face Care</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Skin Care</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Minerals</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Body Care</a></div>
                      </li>
                      <li class="menu-item">
                        <div class="title"> <a href="shop_grid.html">Cosmetic</a></div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="mt-root">
                <div class="mt-root-item"><a href="shop_grid.html">
                  <div class="title title_font"><span class="title-text">New Arrivals</span> </div>
                  </a></div>
              </li>
              <li class="mt-root">
                <div class="mt-root-item"><a href="about_us.html">
                  <div class="title title_font"><span class="title-text">Lookbook</span></div>
                  </a></div>
              </li>
              <li class="mt-root demo_custom_link_cms">
                <div class="mt-root-item"><a href="blog.html">
                  <div class="title title_font"><span class="title-text">Blog</span></div>
                  </a></div>
                <ul class="menu-items col-md-3 col-sm-4 col-xs-12" style="top: 28px; left: 402.531px;">
                  <li class="menu-item depth-1">
                    <div class="title"> <a href="blog_right_sidebar.html"> Blog – Right Sidebar </a></div>
                  </li>
                  <li class="menu-item depth-1">
                    <div class="title"> <a href="blog_left_sidebar.html"> Blog – Left Sidebar </a></div>
                  </li>
                  <li class="menu-item depth-1">
                    <div class="title"> <a href="blog_full_width.html"> Blog – Full-Width </a></div>
                  </li>
                  <li class="menu-item depth-1">
                    <div class="title"> <a href="blog_single_post.html"> Single post </a></div>
                  </li>
                </ul>
              </li>
              <li class="mt-root">
                <div class="mt-root-item">
                  <div class="title title_font"><span class="title-text">Best Seller</span></div>
                </div>
                <ul class="menu-items col-xs-12">
                  <li class="menu-item depth-1 product menucol-1-3 withimage">
                    <div class="product-item">
                      <div class="item-inner">
                        <div class="product-thumbnail">
                          <div class="icon-sale-label sale-left">Sale</div>
                          <div class="pr-img-area"> <a title="Product title here" href="single_product.html">
                            <figure> <img class="first-img" src="images/products/product-1.jpg" alt="html theme"> <img class="hover-img" src="images/products/product-1.jpg" alt="html Template"></figure>
                            </a> </div>
                          <div class="pr-info-area">
                            <div class="pr-button">
                              <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                              <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                              <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a title="Product title here" href="single_product.html">Product title here </a> </div>
                            <div class="item-content">
                              <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                              </div>
                              <div class="pro-action">
                                <button type="button" class="add-to-cart"><span> Add to Cart</span> </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="menu-item depth-1 product menucol-1-3 withimage">
                    <div class="product-item">
                      <div class="item-inner">
                        <div class="product-thumbnail">
                          <div class="icon-sale-label sale-left">Sale</div>
                          <div class="pr-img-area"> <a title="Product title here" href="single_product.html">
                            <figure> <img class="first-img" src="images/products/product-1.jpg" alt="html Template"> <img class="hover-img" src="images/products/product-1.jpg" alt="html Template"></figure>
                            </a> </div>
                          <div class="pr-info-area">
                            <div class="pr-button">
                              <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                              <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                              <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a title="Product title here" href="single_product.html">Product title here </a> </div>
                            <div class="item-content">
                              <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                              </div>
                              <div class="pro-action">
                                <button type="button" class="add-to-cart"><span> Add to Cart</span> </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="menu-item depth-1 product menucol-1-3 withimage">
                    <div class="product-item">
                      <div class="item-inner">
                        <div class="icon-sale-label sale-left">Sale</div>
                        <div class="icon-new-label new-right">New</div>
                        <div class="product-thumbnail">
                          <div class="icon-sale-label sale-left">Sale</div>
                          <div class="pr-img-area"> <a title="Product title here" href="single_product.html">
                            <figure> <img class="first-img" src="images/products/product-1.jpg" alt="html Template"> <img class="hover-img" src="images/products/product-1.jpg" alt="html Template"></figure>
                            </a> </div>
                          <div class="pr-info-area">
                            <div class="pr-button">
                              <div class="mt-button add_to_wishlist"> <a href="wishlist.html"> <i class="fa fa-heart"></i> </a> </div>
                              <div class="mt-button add_to_compare"> <a href="compare.html"> <i class="fa fa-signal"></i> </a> </div>
                              <div class="mt-button quick-view"> <a href="quick_view.html"> <i class="fa fa-search"></i> </a> </div>
                            </div>
                          </div>
                        </div>
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a title="Product title here" href="single_product.html">Product title here </a> </div>
                            <div class="item-content">
                              <div class="rating"> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                              <div class="item-price">
                                <div class="price-box"> <span class="regular-price"> <span class="price">$125.00</span> </span> </div>
                              </div>
                              <div class="pro-action">
                                <button type="button" class="add-to-cart"><span> Add to Cart</span> </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </li> -->
              <li></li>
            </ul>
          </div> 
        </div>
      </div>
    </div>
  </nav>