<!-- mobile menu -->
<div id="mobile-menu">
  <ul>
   @foreach(allCategories() as $category)
      @if($category->parent_id == 0)
        @if(!empty(json_decode(getCategory($category->id))))
        <li><a href="{{route('frontend.product', ['id' => $category->slug])}}">{{$category->name}}</a>
          <ul class="nav">
          <?php 
            foreach(getCategory($category->id) as $sub):
                echo '<li><a href='.url($category->slug.'/'.$sub->slug).'>'.$sub->name.'</a></li>';
            endforeach
          ?>
          </ul>
        </li>
        @else
        <li><a href="{{url($category->slug)}}">{{$category->name}}</a></li>
        @endif
      @endif
    @endforeach
  </ul>
</div>