 <!-- Header -->
  <header>
    <div class="header-container">
       <div class="header-top">
        <div class="container">
          <div class="row">
            <div class="col-sm-4 col-md-4 col-xs-12"> 
              <!-- Default Welcome Message -->
              <div class="welcome-msg hidden-xs hidden-sm">{{setting('welcome_message')}}</div>
              <!-- Language &amp; Currency wrapper  -->
              <div class="language-currency-wrapper">
                <div class="inner-cl">
                  <div class="block block-language form-language">
                    <div class="lg-cur"><!-- <span><img src="{{asset('images/flag-default.png')}}" alt="Indonesia"><span class="lg-id">{{getDefaultLanguage()}}</span><i class="fa fa-angle-down"></i></span> -->{{setting('email')}}</div>
                    <!-- <ul> -->
                      <!-- @foreach(getLanguages() as $lang)
                        <li><a class="selected" href="#"><img src="{{asset('images')}}/{{$lang->flag}}" alt="english"><span>{{$lang->name}}</span></a></li>
                      @endforeach -->
                    <!-- </ul> -->
                  </div>
                  <!-- <div class="block block-currency">
                    <div class="item-cur"><span>USD</span><i class="fa fa-angle-down"></i></div>
                    <ul>
                      <li><a href="#"><span class="cur_icon">€</span>EUR</a></li>
                      <li><a href="#"><span class="cur_icon">¥</span>JPY</a></li>
                      <li><a class="selected" href="#"><span class="cur_icon">$</span>USD</a></li>
                    </ul>
                  </div> -->
                </div>
              </div>
            </div>
            <!-- top links -->
            <div class="headerlinkmenu col-md-8 col-sm-8 col-xs-12"> <span class="phone  hidden-xs hidden-sm">Call Us: {{setting('phone')}}</span>
              <ul class="links">
                <li class="hidden-xs"><a title="Cara Memesan" href="{{route('howOrder')}}"><span>Cara Memesan</span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- header inner -->
      <div class="header-inner">
        <div class="container">
          <div class="row">
            <div class="col-sm-3 col-xs-12 jtv-logo-block"> 
              <!-- Header Logo -->
              <div class="logo"><a title="e-commerce" href="{{route('frontend.home')}}"><img alt="ShopMart" title="ShopMart" src="{{asset('images/logo.png')}}"></a> </div>
            </div>
            <div class="col-xs-12 col-sm-9 col-sm-push-1 col-md-6 jtv-top-search"> 
              <!-- Search -->
              <div class="top-search">
                <div id="search">
                  <form action="{{url('search')}}" method="get">
                    <div class="input-group">
                      <select class="cate-dropdown hidden-xs hidden-sm" name="category">
                        <option value="all">All Categories</option>
                        @foreach(allCategories() as $category)
                          @if($category->parent_id == 0)
                            <option value="{{$category->slug}}">{{$category->name}}</option>
                            <?php 
                              foreach(getCategory($category->id) as $sub):
                                  echo '<option value='.$sub->slug.'>&nbsp;&nbsp;&nbsp; '.$sub->name.' </option>';
                              endforeach
                            ?>
                          @endif
                        @endforeach
                      </select>
                      <input type="text" class="form-control" placeholder="Enter your search..." name="word" value="{{(request()->word != '') ? request()->word : ''}}">
                      <button class="btn-search" type="submit"><i class="fa fa-search"></i></button>
                  </form>
                    </div>
                </div>
              </div>
              <!-- End Search --> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- end header -->