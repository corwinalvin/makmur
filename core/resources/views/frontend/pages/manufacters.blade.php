@extends('frontend.master')
@section('title', 'Manufacters')
@section('content')
<!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{route('frontend.home')}}">Home</a><span>&raquo;</span></li>
            <li> <a  href="javascript:void(0)">Brands</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<!-- Breadcrumbs End --> 
<section class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        <div class="col-main col-sm-12 col-xs-12">
          <div class="my-account">
            <div class="page-title">
              <h2>Manufacturers</h2>
            </div>
            <div class="manufacturers-box">
              <div class="row">
              	@foreach($manufacters as $manufacter)
	                <figure class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> <a href="{{route('frontend.product', ['id' => strtolower($manufacter->brand)])}}" class="jtv-m-logo-img"> <img  src="{{url(json_decode($manufacter->logo))}}" alt="{{$manufacter->brand}}"> </a>
	                  <figcaption>
	                    <h5><a href="{{route('frontend.product', ['id' => strtolower($manufacter->brand)])}}">{{$manufacter->brand}}</a></h5>
	                  </figcaption>
	                </figure>
              	@endforeach
              </div>
            </div>
          </div>
        </div>
   <!--      <aside class="right sidebar col-sm-3 col-xs-12">
          <div class="sidebar-account block">
            <div class="sidebar-bar-title">
              <h3>My Account</h3>
            </div>
            <div class="block-content">
              <ul>
                <li><a>Account Dashboard</a></li>
                <li><a href="#">Account Information</a></li>
                <li><a href="#">Address Book</a></li>
                <li><a href="#">My Orders</a></li>
                <li><a href="#">Billing Agreements</a></li>
                <li><a href="#">Recurring Profiles</a></li>
                <li><a href="#">My Product Reviews</a></li>
                <li><a href="#">My Tags</a></li>
                <li class="current"><a href="#">My Wishlist</a></li>
                <li><a href="#">My Downloadable</a></li>
                <li class="last"><a href="#">Newsletter Subscriptions</a></li>
              </ul>
            </div>
          </div>
          <div class="compare block">
            <div class="sidebar-bar-title">
              <h3>Compare Products (2)</h3>
            </div>
            <div class="block-content">
              <ol id="compare-items">
                <li class="item"> <a href="#" title="Remove This Item" class="remove-cart"><i class="icon-close"></i></a> <a href="#" class="product-name">Vestibulum porta tristique porttitor.</a> </li>
                <li class="item"> <a href="#" title="Remove This Item" class="remove-cart"><i class="icon-close"></i></a> <a href="#" class="product-name">Lorem ipsum dolor sit amet</a> </li>
              </ol>
              <div class="ajax-checkout">
                <button type="submit" title="Submit" class="button button-compare"> <span> Compare</span></button>
                <button type="submit" title="Submit" class="button button-clear"> <span> Clear All</span></button>
              </div>
            </div>
          </div>
        </aside> -->
      </div>
    </div>
  </section>
@stop