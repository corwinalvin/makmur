@extends('frontend.master', ['staticbar' => true])
@section('title', 'Category')
@section('content')  
<!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{route('frontend.home')}}">Home</a><span>&raquo;</span></li>
            @php
              $title = null;
              if(null !== Request::segment(2)){
                $s = getCategoryBySlug(Request::segment(2));
                $a = getCategoryBySlug(Request::segment(1));
              }else {
                $s = getCategoryBySlug(Request::segment(1));
              }
            @endphp 
            @if(!Request::is('search*'))
            @if($s[0]['parent'] == '' && $s[0]['cat'] == '')
            <li> <a href="{{route('manufacters')}}">Brand</a><span>&raquo;</span></li>
            <li> <a href="javascript:void(0)"><strong>{{ucwords(Request::segment(1))}}</a></strong></li>
            @elseif ($s[0]['parent'] == '0')
            <li> <a href="javascript:void(0)"><strong>{{$title = $s[0]['cat']->name}}</a></strong></li>
            @else
            <li> <a href="{{route('frontend.product', ['id' => $a[0]['cat']->slug])}}">{{$a[0]['cat']->name}}<span>&raquo;</span></a></li>
            <li> <a href="javascript:void(0)"><strong>{{$title = $s[0]['cat']->name}}</strong></a></li>
            @endif
            @else
            <li> <a href="javascript:void(0)">Search<span>&raquo;</span></a></li>
            <li> <a href="javascript:void(0)"><strong>{{$title = request()->word}}</strong></a></li>
            @endif
          </ul>
        </div>
      </div>
    </div>
  </div>
<!-- Breadcrumbs End --> 
 <!-- Main Container -->
  <div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">
        <aside class="sidebar col-sm-2 col-xs-12">
          <div class="block shop-by-side">
            <div class="block-content">
              <div class="manufacturer-area">
                <h5 class="saider-bar-title">Select Category</h5>
                <div class="saide-bar-menu">
                  <ul>
                    @php 
                      $parent_id = 0;
                    @endphp
                    @foreach(allCategories() as $category)
                      @if($category->parent_id == 0)
                        @if(!empty(json_decode(getCategory($category->id))))
                          @php
                            if(Request::segment(1) == $category->slug){
                              $parent_id = $category->id;
                            }
                          @endphp
                        <li ><a {{(Request::segment(1) == $category->slug) ? 'class=active' : ''}} href="{{route('frontend.product', ['id' => $category->slug])}}"> {{$category->name}} ({{$category->count}})</a></li>
                        @else
                        @php
                            if(Request::segment(1) == $category->slug){
                              $parent_id = $category->id;
                            }
                          @endphp
                        <li><a {{(Request::segment(1) == $category->slug) ? 'class=active' : ''}} href="{{url($category->slug)}}">{{$category->name}} ({{$category->count}})</a></li>
                        @endif
                      @endif
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
          </div>  
        </aside>
      @if(!Request::is('search*'))
        @if($s[0]['parent'] == '' && $s[0]['cat'] == '')
          <div class="col-main col-sm-10 col-xs-12">
        @else
        <aside class="sidebar col-sm-2 col-xs-12">
          <div class="block shop-by-side">
            <div class="block-content">
              <div class="manufacturer-area">
                <h5 class="saider-bar-title">Sub Categories</h5>
                <div class="saide-bar-menu">
                  <ul>
                    @forelse(getSubCate($parent_id) as $cate)
                        <li><a {{(Request::segment(2) == $cate->slug) ? 'class=active' : ''}} href="{{url(Request::segment(1).'/'.$cate->slug)}}">{{$cate->name}}  ({{$cate->count}})</a></li>
                    @empty
                        <li>(No Sub Category)</li>
                    @endforelse
                  </ul>
                </div>
              </div>
            </div>
          </div>  
        </aside>
          <div class="col-main col-sm-8 col-xs-12">
        @endif
      @else
          <div class="col-main col-sm-10 col-xs-12">
      @endif
           <div class="shop-inner">
            <div class="page-title">
            @if($s[0]['parent'] == '' && $s[0]['cat'] == '')
              <h1>{{ucwords(Request::segment(1))}}</h1>
            @else
              <h1>{{$title}}</h1>
            @endif
            </div>
            <div class="toolbar column">
              <div class="sorter">
                <div class="short-by">
                  @php
                    $limit = Request::get('limit');
                    $so = Request::get('sort_by');
                    $ob = Request::get('order_by');
                  @endphp
                  <label>Sort By:</label>
                  <select id="sort-by">
                    <option {{($so == 'name' && $ob == 'asc') ? 'selected="selected' : '' }}" value="&sort_by=name&order_by=asc">Name (A-Z)</option>
                    <option {{($so == 'name' && $ob == 'desc') ? 'selected="selected' : '' }}" value="&sort_by=name&order_by=desc">Name (Z-A)</option>
                    <option {{($so == 'review' && $ob == 'asc') ? 'selected="selected' : '' }}" value="&sort_by=review&order_by=asc">Review</option>
                   <!--  <option {{($so == 'price' && $ob == 'desc') ? 'selected="selected' : '' }}" value="&sort_by=price&order_by=desc">Highest Price</option>
                    <option {{($so == 'price' && $ob == 'asc') ? 'selected="selected' : '' }}" value="&sort_by=price&order_by=asc">Lowest Price</option> -->
                    <option {{($so == 'created_at' && $ob == 'asc') ? 'selected="selected' : '' }}" value="&sort_by=created_at&order_by=asc">Latest</option>
                    <option {{($so == 'created_at' && $ob == 'desc') ? 'selected="selected' : '' }}" value="&sort_by=created_at&order_by=desc">Newest</option>
                  </select>
                </div>
                <div class="short-by page">
                  <label>Show:</label>
                  <select id="limit-page">
                    <option {{($limit == 12) ? 'selected="selected' : '' }}">12</option>
                    <option {{($limit == 20) ? 'selected="selected' : '' }}">20</option>
                    <option {{($limit == 32) ? 'selected="selected' : '' }}">32</option>
                    <option {{($limit == 40) ? 'selected="selected' : '' }}">40</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="product-grid-area">
              <ul class="products-grid">
                @foreach($items as $item)
                <li class="item col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                  <div class="product-item">
                    <div class="item-inner">
                      <div class="product-thumbnail">
                        <div class="pr-img-area"> <a title="{{$item->name}}" href="{{route('frontend.product', ['id' => $item->slug])}}">
                          <figure> <img onerror="imgError(this);" alt="{{$item->name}}" src="{{url(json_decode($item->pictures)[0])}}" alt="HTML template"> <img alt="{{$item->name}}"  class="hover-img" onerror="imgError(this);" src="{{url(json_decode($item->pictures)[0])}}" alt="HTML template"></figure>
                          </a> 
                      <div class="item-info">
                        <div class="info-inner">
                          <div class="item-title"> 
                      <p>{{ucwords($item->brands->brand)}}</p>
                      <a title="{{$item->name}}" href="{{route('frontend.product', ['id' => $item->slug])}}"><span></span>{{ucwords($item->name)}} </a> 
                          </div>
                          <div class="item-content">
                            <div class="rating" style="background-color: #eee;"> 
                              @if(5-$item->review > 0)
                              @for ($i = 0; $i < $item->review; $i++)
                                  <i class="fa fa-star">  </i>
                              @endfor
                              @for ($i = 0; $i < 5-$item->review; $i++)
                                <i class="fa fa-star-o">  </i>
                              @endfor
                              @else
                              Belum Direview
                              @endif
                            </div>
                            @if($item->show_price == 1)
                            <div class="item-price">
                              <div class="price-box"> <span class="regular-price"> <span class="price">IDR{{number_format($item->price)}}</span> </span> </div>
                            </div>
                            @endif
                          </div>
                        </div>
                      </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                @endforeach
              </ul>
              @if($items->count() == 0)
              <p align="center">Maaf Produk Tidak Ditemukan</p>
              @endif
            </div>
            <div class="pagination-area ">
              {{$items->links()}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Main Container End --> 
@endsection
@section('js')
<script>
  $('#limit-page').change(function(){
    <?php 
        $sort = 'name';
        $order = 'asc';
          if(Request::has('sort_by')){
              $sort = Request::get('sort_by');
          }

          if(Request::has('order_by')){
            $order = Request::get('order_by');
          }
     ?>
     window.location.href="{{Request::url()}}?limit="+$(this).val()+"&sort_by={{$sort}}&order_by={{$order}}";
  });

  $('#sort-by').change(function(){
    <?php 
        $limits = 12;
          if(Request::has('limit')){
              $limits = Request::get('limit');
          }
    ?>
    window.location.href="{{Request::url()}}?limit={{$limits}}"+$(this).val()+"";
  });
</script>
@endsection