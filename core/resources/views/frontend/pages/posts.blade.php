@extends('frontend.master')
@section('title', 'Post')
@section('content')
<!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{route('frontend.home')}}">Home</a><span>&raquo;</span></li>
            @if(Request::is('cara-pemesanan'))
            <li> <a  href="javascript:void(0)">Cara Pemesanan</a></li>
            @else
            <li> <a  href="javascript:void(0)">Posts<span>&raquo;</span></a></li>
            <li> <a  href="javascript:void(0)"><strong>{{getPost(Request::segment(3))}}</strong></a></li>
            @endif
          </ul>
        </div>
      </div>
    </div>
  </div>
<!-- Breadcrumbs End --> 
 <!-- Main Container -->
  <section class="blog_post">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-9">
          <div class="entry-detail">
            <div class="page-title">
                  </div>
            <div class="entry-photo">
              <figure><img src="{{url($post->thumbnail)}}" alt="{{$post->title}}" class="img img-responsive"></figure>
            </div>
            <div class="entry-meta-data"> 
            <div class="blog-top-desc">
                <div class="blog-date"> {{date('d M Y', strtotime($post->created_at))}} </div>
                  <h1><a href="{{route('posts.detail', ['id' => $post->id])}}">{{$post->title}}</a></h1>
                  <div class="jtv-entry-meta"> <i class="fa fa-user-o"></i> <strong>Admin</strong></div>
                </div>
            </div>
            <div class="content-text clearfix">
              {!!$post->description!!}
            </div>
            </div>
          </div>
                  <!-- right colunm -->
        <aside class="sidebar col-xs-12 col-sm-3"> 
          <!-- Popular Posts -->
          <div class="block blog-module">
            <div class="sidebar-bar-title">
              <h3>Popular Posts</h3>
            </div>
            <div class="block_content"> 
              <!-- layered -->
              <div class="layered">
                <div class="layered-content">
                  <ul class="blog-list-sidebar">
                    @foreach($posts as $artikel)
                    <li>
                      <div class="post-thumb"> <a href="{{route('posts.detail', ['id' => $artikel->id])}}"><img src="{{url($artikel->thumbnail)}}" alt="{{$artikel->title}}"></a> </div>
                      <div class="post-info">
                        <h5 class="entry_title"><a href="{{route('posts.detail', ['id' => $artikel->id])}}">{{$artikel->title}}</a></h5>
                        <div class="post-meta"> <span class="date"><i class="pe-7s-date"></i> {{date('Y-m-d', strtotime($artikel->created_at))}}</span></div>
                      </div>
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              <!-- ./layered --> 
            </div>
          </div>
          <!-- ./Popular Posts --> 
          <!-- ./tags --> 
        </aside>
        <!-- ./right colunm --> 
        </div>
      </div>
    </div>
  </section>
  <!-- Main Container End --> 
@endsection