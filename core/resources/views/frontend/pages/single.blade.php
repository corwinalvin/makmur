@extends('frontend.master')
@section('title', 'Detail Product')
@section('content')
<!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{route('frontend.home')}}">Home</a><span>&raquo;</span></li>
            @if(getCategoryBySlug($item->category)[0]['parent'] != '0')
            <li> <a  href="{{route('frontend.product',['id' => getCategoryBySlug($item->category)[0]['parent']->slug])}}">{{getCategoryBySlug($item->category)[0]['parent']->name}}<span>&raquo;</span></a></li>
            @endif
            @if($cat = getCategoryBySlug($item->category)[0]['cat']->name)
            <li> <a  href="{{route('frontend.product',['id' => getCategoryBySlug($item->category)[0]['cat']->slug])}}">{{$cat}}<span>&raquo;</span></a></li>
            @endif
            <li> <a  href="javascript:void(0)"><strong>{{ucwords($item->name)}}</strong></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<!-- Breadcrumbs End --> 
<!-- Main Container -->
  <div class="main-container col1-layout">
    <div class="container">
      <div class="row">
        <aside class="sidebar col-sm-2 col-xs-12">
          <div class="block shop-by-side">
            <div class="block-content">
              <div class="manufacturer-area">
                <h5 class="saider-bar-title">Select Category</h5>
                <div class="saide-bar-menu">
                  <ul>
                    @php 
                      $parent_id = 0;
                    @endphp
                    @foreach(allCategories() as $category)
                      @if($category->parent_id == 0)
                        @if(!empty(json_decode(getCategory($category->id))))
                          <?php
                            if(getCategoryBySlug($item->category)[0]['parent'] != '0' ){
                              if(getCategoryBySlug($item->category)[0]['parent']->slug == $category->slug){
                                $parent_id = $category->id;
                                $slug = $category->slug;
                              }
                            }else{
                              if(getCategoryBySlug($item->category)[0]['cat']->slug == $category->slug){
                                $parent_id = $category->id;
                                $slug = $category->slug;
                              }
                            }
                          ?>
                        <li ><a {{($parent_id == $category->id) ? 'class=active' : ''}} href="{{route('frontend.product', ['id' => $category->slug])}}"> {{$category->name}} ({{$category->count}})</a></li>
                        @else
                        @php
                            if(getCategoryBySlug($item->category)[0]['cat']->slug == $category->slug){
                              $parent_id = $category->id;
                              $slug = $category->slug;
                            }
                          @endphp
                        <li><a {{(getCategoryBySlug($item->category)[0]['cat']->slug == $category->slug) ? 'class=active' : ''}} href="{{url($category->slug)}}">{{$category->name}} ({{$category->count}})</a></li>
                        @endif
                      @endif
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
          </div>  
        </aside>
        <aside class="sidebar col-sm-2 col-xs-12">
          <div class="block shop-by-side">
            <div class="block-content">
              <div class="manufacturer-area">
                <h5 class="saider-bar-title">Sub Categories</h5>
                <div class="saide-bar-menu">
                  <ul>
                    @foreach(getSubCate($parent_id) as $cate)
                        <li><a {{(getCategoryBySlug($item->category)[0]['cat']->slug == $cate->slug) ? 'class=active' : ''}} href="{{url($slug.'/'.$cate->slug)}}">{{$cate->name}} ({{$cate->count}})</a></li>
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
          </div>  
        </aside>
        <div class="col-main">
          <div class="product-view-area">
              <div class="product-name">
                <h1>{{$item->name}}</h1>
              </div>
            <div class="product-big-image col-xs-8 col-sm-8 col-lg-8 col-md-8">
              <div class="large-image" align="center"> <a href="{{url(json_decode($item->pictures)[0])}}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img class="zoom-img" src="{{url(json_decode($item->pictures)[0])}}" alt="{{$item->name}}"> </a> 
              </div>
              @if(json_decode($item->pictures)[0] != "noproduct.png")
              <div class="flexslider flexslider-thumb">
                <ul class="previews-list slides">
                @if(count(json_decode($item->pictures)) >= 3)
                  @foreach(json_decode($item->pictures) as $pic)
                  <li style="max-height:53px;"><a href='{{url($pic)}}' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{{url($pic)}}' "><img alt="{{$item->name}}" src="{{url($pic)}}" /></a></li>
                  @endforeach
                @else
                  @foreach(json_decode($item->pictures) as $pic)
                  <li style="max-height: 60px;"><a href='{{url($pic)}}' class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{{url($pic)}}' "><img alt="{{$item->name}}" style="max-width: 100px; " src="{{url($pic)}}"/></a></li>
                  @endforeach
                @endif
                </ul>
              </div>
              @endif
              <!-- end: more-images --> 
            </div>
            <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 product-details-area">
              <div class="ratings">
                <div class="rating"> 
                @if(5-$item->review > 0)
                @for ($i = 0; $i < $item->review; $i++)
                  <i class="fa fa-star">  </i>
                @endfor
                @for ($i = 0; $i < 5-$item->review; $i++)
                  <i class="fa fa-star-o">  </i>
                @endfor
                @else
                <p></p>
                @endif
                </div>
                @if($item->stock >= 1)
                    @if($item->show_price == 1)
              <div class="price-box pull-left">
                <p class="special-price"> <span class="price-label">Price</span> <span class="price"> IDR{{number_format($item->price)}} </span> </p>
              </div>
                @endif
                <p class="availability in-stock pull-right">Availability: <span>In Stock</span></p>
                @else
                    @if($item->show_price == 1)
              <div class="price-box pull-left">
                <p class="special-price"> <span class="price-label">Price</span> <span class="price"> IDR{{number_format($item->price)}} </span> </p>
              </div>
                @endif
                <p class="availability out-of-stock pull-right">Availability: <span>Not In Stock</span></p>
                @endif
              </div>
              <div class="short-description">
                <h2>Description</h2>
                <div class="std">{!!$item->description!!}</div>
              </div>
              <hr>
              <div class="single-box" align="center">
                  <p></p>
                  @if(!empty($item->brosur))
                  <a href="{{route('downloadPDF', ['id' => $item->id])}}" target="_blank" class="btn btn-danger">Download 
                  Brochure</a>
                  @endif
                  <a href="{{route('frontend.contact')}}?name={{$item->name}}&item_url={{Request::url()}}" class="btn btn-warning">Contact Us</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Main Container End --> 
  
  <!-- Related Product Slider -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="related-product-area">
          <div class="page-header">
            <h2>Related Products</h2>
          </div>
          <div class="related-products-pro">
            <div class="slider-items-products">
              <div id="related-product-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col4 fadeInUp">
                @foreach($rItems as $rItem)
                  <div class="product-item">
                    <div class="item-inner">
                      <div class="product-thumbnail">
                        <div class="pr-img-area"> <a title="{{$rItem->name}}" href="{{route('frontend.product', ['id' => $rItem->slug])}}">
                          <figure> <img onerror="imgError(this);" class="first-img" src="{{url(json_decode($rItem->pictures)[0])}}" alt="{{$rItem->name}}"> <img onerror="imgError(this);" class="hover-img" src="{{url(json_decode($rItem->pictures)[0])}}"></figure>
                          </a> </div>
                      </div>
                      <div class="item-info">
                        <div class="info-inner">
                          <div class="item-title"> <a title="{{$rItem->name}}" href="{{route('frontend.product', ['id' => $rItem->slug])}}">{{$rItem->name}} </a> </div>
                          <div class="item-content">
                            <div class="rating"> 
                              @if(5-$item->review > 0)
                              @for ($i = 0; $i < $item->review; $i++)
                                  <i class="fa fa-star">  </i>
                              @endfor
                              @for ($i = 0; $i < 5-$item->review; $i++)
                                <i class="fa fa-star-o">  </i>
                              @endfor
                              @endif
                            </div>
                              @if($rItem->show_price == 1)
                            <div class="item-price">
                              <div class="price-box"> <span class="regular-price"> <span class="price">IDR{{number_format($rItem->price)}}</span> </span> </div>
                            </div>
                              @endif
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection