@extends('frontend.master', ['secondarybar' => false])
@section('title', setting('site_name'))
@section('content')
  <!-- SLIDE SHOW -->
  <div class="main-slider" id="home">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12 banner-left hidden-xs">
        <img src="images/banner-left.jpg" alt="Makmur Diesel">
        </div>
        <div class="col-sm-9 col-md-9 col-lg-9 col-xs-12 jtv-slideshow">
          <div id="jtv-slideshow">
            <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container' >
              <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
                <ul>
                  @foreach(getSliders() as $slider)
                  <li onclick="window.location.href='{{$slider->link}}'" data-transition='fade' data-slotamount='7' data-masterspeed='1000' data-thumb=''><img alt="Makmur Diesel" onclick="window.location.href='{{$slider->link}}'" src='{{url($slider->gambar)}}' data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/></li>
                  @endforeach
                </ul>
                <div class="tp-bannertimer"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END SLIDE SHOW -->
  <!-- service section -->
  <div class="jtv-service-area">
    <div class="container">
      <div class="row">
        <div class="col col-md-3 col-sm-6 col-xs-12">
          <div class="block-wrapper ship">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-paper-plane"></i></div>
              <div class="service-wrapper">
                <h3>{{setting('judul_pesawat')}}</h3>
                <p>{{setting('pesawat_deskripsi')}}</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col col-md-3 col-sm-6 col-xs-12 ">
          <div class="block-wrapper return">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-rotate-right"></i></div>
              <div class="service-wrapper">
                <h3>{{setting('panah_judul')}}</h3>
                <p>{{setting('panah_deskripsi')}}</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col col-md-3 col-sm-6 col-xs-12">
          <div class="block-wrapper support">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-umbrella"></i></div>
              <div class="service-wrapper">
                <h3>{{setting('judul_payung')}}</h3>
                <p>{{setting('payung_deskripsi')}}</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col col-md-3 col-sm-6 col-xs-12">
          <div class="block-wrapper user">
            <div class="text-des">
              <div class="icon-wrapper"><i class="fa fa-tags"></i></div>
              <div class="service-wrapper">
                <h3>{{setting('judul_tag')}}</h3>
                <p>{{setting('tag_deskripsi')}}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end service section -->
  
@endsection