@extends('frontend.master', ['secondarybar' => false])
@section('title', 'Contact Us!')
@section('content')
<!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{route('frontend.home')}}">Home</a><span>&raquo;</span></li>
            <li> <a  href="javascript:void(0)"><strong>Contact Us</strong></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<!-- Breadcrumbs End --> 
  <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="row">
        <section class="col-main col-sm-12">
          <div id="contact" class="page-content page-contact">
            <div class="page-title">
              <h2>Contact Us</h2>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6" id="contact_form_map">
                <h3 class="page-subheading">Let's get in touch</h3>
                <ul class="store_info">
                  <li><i class="fa fa-home"></i>{{setting('address')}}</li>
                  <li><i class="fa fa-phone"></i><span>{{setting('phone')}}</span></li>
                  <li><i class="fa fa-print"></i><span>{{setting('phone_2')}}</span></li>
                  <li><i class="fa fa-envelope"></i>Email: <span><a href="mailto:support@justtheme.com">{{setting('email')}}</a></span></li>
                </ul>
                <br/>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3965.65556046925!2d107.3003497!3d-6.3089017!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6977db64a7542f%3A0xa2a04377135d2abe!2sMakmur+Diesel!5e0!3m2!1sid!2sid!4v1507724573349" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>
              <div class="col-sm-6">
                <h3 class="page-subheading">Make an enquiry</h3>
                @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @include('flash::message')
                <form action="{{route('frontend.contact.store')}}" method="post">
                {{csrf_field()}}
                <div class="contact-form-box">
                  <div class="form-selector">
                    <label>Name</label>
                    <input type="text" class="form-control input-sm" id="name" name="name" value="{{old('name')}}" />
                  </div>
                  <div class="form-selector">
                    <label>Email</label>
                    <input type="text" class="form-control input-sm" id="email" name="email" value="{{old('email')}}" />
                  </div>
                  <div class="form-selector">
                    <label>Subject</label>
                    <input type="text" class="form-control input-sm" id="subject" name="subject" value="{{old('subject')}}" />
                  </div>
                  <div class="form-selector">
                    <label>Message</label>
                    @if(empty(old('message')) && !empty(request()->name))
                      <textarea class="form-control input-sm" id="message" name="message" rows="5" cols="30">[ASK]{{request()->name}}{{request()->item_url}}
                      </textarea>
                    @else
                      <textarea class="form-control input-sm" id="message" name="message" rows="5" cols="30">{{old('message')}}</textarea>
                    @endif
                  </div>
                  <div class="form-selector">
                    <button class="button"><i class="icon-paper-plane icons"></i>&nbsp; <span>Send Message</span></button>
                </form>
                    &nbsp; <a href="javascript:void(0)" class="button clear">Clear</a> </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
  <!-- Main Container End --> 
@endsection
@section('js')
<script>
$('.clear').click(function(){
   $("#name").val('');
   $("#email").val('');
   $("#subject").val('');
   $("#message").val('');
});
</script>
@stop
