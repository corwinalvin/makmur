@extends('frontend.master', ['secondarybar' => false])
@section('title', 'About Us')
@section('content')
<!-- Breadcrumbs -->
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a title="Go to Home Page" href="{{route('frontend.home')}}">Home</a><span>&raquo;</span></li>
            <li> <a href="javascript:void(0)"><strong>About Us</strong></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<!-- Breadcrumbs End --> 
<div class="main container">
     <div class="about-page">
        <div class="col-xs-12 col-sm-6"> 
          
          <h1><span class="text_color">{{setting('site_name')}}</span></h1>
          <p>{!!section('about_us', 'content')!!}</p>
          <ul>
            <li><i class="fa fa-arrow-right"></i>&nbsp; 
                <a type="button" data-toggle="collapse" data-target="#visimisi">Visi dan Misi</a>
                <p id="visimisi" class="collapse" style="margin-left: 25px;">{{setting('visi_misi')}}</p>
            </li>
            <li><i class="fa fa-arrow-right"></i>&nbsp; <a type="button" data-toggle="collapse" data-target="#visi">Visi</a>
                <p id="visi" class="collapse" style="margin-left: 25px;">{{setting('visi')}}</p>
            </li>
            <li><i class="fa fa-arrow-right"></i>&nbsp; <a type="button" data-toggle="collapse" data-target="#misi">Misi</a>
                <div id="misi" class="collapse" style="margin-left: 25px !important;">{!!setting('misi')!!}</div></li>
            <li><i class="fa fa-arrow-right"></i>&nbsp; <a type="button" data-toggle="collapse" data-target="#km">Kebijakan Mutu Perusahaan</a>
                <div id="km" class="collapse" style="margin-left: 25px !important;">{!!setting('kebijakan_mutu_perusahaan')!!}</div></li>
          </ul>
        </div>
        <div class="col-xs-12 col-sm-6">
          <div class="item active"> <img src="{{url(setting('about_picture'))}}" class="img img-thumbnail" alt="About Makmur Diesel" > </div>
        </div>
      </div>

  </div>
@stop