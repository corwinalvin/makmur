<!-- Name Field -->
<div class="form-group col-md-3">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $contact->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group col-md-3">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $contact->email !!}</p>
</div>

<!-- subject Field -->
<div class="form-group col-md-3">
    {!! Form::label('subject', 'Subject:') !!}
    <p>{!! $contact->subject !!}</p>
</div>

<!-- Details Field -->
<div class="form-group col-md-3">
    {!! Form::label('details', 'Details:') !!}
    <p>{!! $contact->details !!}</p>
</div>
<!-- Message Field -->
<div class="form-group col-md-12">
    {!! Form::label('message', 'Message:') !!}
    <p>{!! $contact->message !!}</p>
</div>


