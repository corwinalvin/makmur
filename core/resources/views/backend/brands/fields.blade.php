<!-- Brand Field -->
<div class="form-group col-sm-6">
    {!! Form::label('brand', 'Brand:') !!}
    {!! Form::text('brand', null, ['class' => 'form-control']) !!}
</div>
<!-- Logo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo', 'Logo:') !!}
     <div class="input-group">
       <span class="input-group-btn">
         <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
           <i class="fa fa-file-o"></i> Choose
         </a>
       </span>
       @if(isset($brands->logo) && $brands->logo != "")
        <input id="thumbnail" class="form-control" type="text" value="{{json_decode($brands->logo)}}" name="logo" required="">
       @else
        <input id="thumbnail" class="form-control" type="text" name="logo" required="">
       @endif
     </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('brands.index') !!}" class="btn btn-default">Cancel</a>
</div>

<div class="form-group col-sm-12">
     <p>
     @if(isset($brands->logo) && $brands->logo != "")
		<img src="{{url(json_decode($brands->logo))}}" width="50%">
     @endif
     </p>
 </div>
@section('scripts')
<script>
$('#lfm').filemanager('image', {
    prefix: '{{url("/")}}/laravel-filemanager'
});
</script>
@stop
