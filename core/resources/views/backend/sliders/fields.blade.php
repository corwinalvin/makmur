<style>
.image-chooser {
    margin-top: 20px;
}
.image-chooser .item {
    display: inline-block;
    float: left;
    width: 100px;
    height: 100px;
    border: 1px solid #ddd;
    background-color: #f2f2f2;
    text-align: center;
    cursor: pointer;
    transition: all .5s;
    margin-right: 10px;
    overflow: hidden;
    position: relative;
    margin-bottom: 10px;
}

.image-chooser .item img {
    width: 100%;
    display: inline-block;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
}

.image-chooser .item:hover {
    opacity: .8;
}

.image-chooser .item .ion {
    margin-top: 22px;
    font-size: 30px;
    display: block;
}

.image-chooser .item .close {
    position: absolute;
    top: 0;
    right: 0;
    background-color: #fff;
    padding: 5px;
    opacity: 1;
}

.image-chooser .item .close .ion {
    margin: 0;
    font-size: 16px;
}
</style>
<!-- Gambar Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('gambar', 'Gambar:') !!}
   	<div id="selected_image" class="image-chooser">
        <div class="image-chooser-list"></div>
        <div class="toggle item">
            <i class="ion ion-plus"></i>
            Add Image
        </div>
    </div>
</div>

<!-- Link Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('link', 'Link:') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('sliders.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
<script>
var count = 0;
$(".image-chooser").each(function(){
    var $this = $(this);
    $(this).find(".toggle").click(function(){
        image_chooser_add($this);
    });

    @if(isset($sliders->gambar))
        image_chooser_add($this, '{{url($sliders->gambar)}}');
    @endif
});
function image_chooser_add($this, item) {
    count ++;
    if(!item) {
        item = null;
    }
    // if(count == 1 ){
    element = '<div class="item ui-state-default" data-fm="true" data-input="picture'+count+'" data-preview="image-chooser-preview'+count+'"><img id="image-chooser-preview'+count+'" '+(item ? 'src="'+item+'"':'')+'><i class="ion ion-image"></i>Pick Image<input type="hidden" name="gambar" id="picture'+count+'" '+(item ? 'value="'+item+'"' : '')+'><div class="close" onclick="$(this).parent().remove()"><i class="ion ion-close"></i></div></div>';
	    $this.find(".image-chooser-list").append(element);
	    $('[data-fm="true"]').filemanager('image', {
	        prefix: '{{url("/")}}/laravel-filemanager'
	    });
    // }
}
</script>
@stop