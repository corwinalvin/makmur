<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => "Makmur Diesel", // set false to total remove
            'description'  => 'Makmur Diesel', // set false to total remove
            'separator'    => ' - ',
            'language'     => 'Betawi-Indonesia',
            'author'       => 'Makmur Diesel',
            'revisit-after'=> '7',
            'webcrawlers'  => 'all',
            'rating'       => 'general',
            'spiders'      => 'all',
            'keywords'     => ['Makmur Diesel'],
            'canonical'    => false, // Set null for using Url::current(), set false to total remove
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => 'Makmur Diesel', // set false to total remove
            'description' => 'Makmur Diesel', // set false to total remove
            'url'         => false, // Set null for using Url::current(), set false to total remove
            'type'        => false,
            'site_name'   => false,
            'images'      => [],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
          //'card'        => 'summary',
          //'site'        => '@LuizVinicius73',
        ],
    ],
];
